; Brainfuck JIT compiler (not an optimizing one)
; Sample program requires text view

.define
	; Memory layout
	jit_dst 0000h
	bf_mem 1000h
	text_out E000h
	program_text F000h
	assembler_code F800h

.data program_text
	programa:
	;db 2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh
	;db 2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh
	;db 2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh
	;db 2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2eh
	db 22h,3eh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,5bh,3ch,2bh,2bh,2bh,2bh,2bh
	db 2bh,2bh,2bh,3eh,2dh,5dh,3ch,2eh,3eh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,5bh,3ch
	db 2bh,2bh,2bh,2bh,3eh,2dh,5dh,3ch,2bh,2eh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2eh
	db 2eh,2bh,2bh,2bh,2eh,5bh,2dh,5dh,3eh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,5bh
	db 3ch,2bh,2bh,2bh,2bh,3eh,2dh,5dh,20h,3ch,2eh,3eh,2bh,2bh,2bh,2bh,2bh,2bh
	db 2bh,2bh,2bh,2bh,2bh,5bh,3ch,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,3eh,2dh,5dh
	db 3ch,2dh,2eh,2dh,2dh,2dh,2dh,2dh,2dh,2dh,2dh,2eh,2bh,2bh,2bh,2eh,2dh,2dh
	db 2dh,2dh,2dh,2dh,2eh,2dh,2dh,2dh,2dh,2dh,2dh,2dh,2dh,2eh,5bh,2dh,5dh,3eh
	db 2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,5bh,3ch,2bh,2bh,2bh,2bh,3eh,2dh,20h,5dh
	db 3ch,2bh,2eh,5bh,2dh,5dh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2bh,2eh
	db 0h

.org assembler_code
main:
	; Assemble program
	lxi D, programa
	lxi H, jit_dst
	call bfasm

	; Execute program
	lxi D, text_out
	lxi H, bf_mem
	jmp jit_dst

; BRAINFUCK ASSEMBLER FUNCTION
; Inputs:
; - DE: [Input] Pointer into Brainfuck source code (terminated by zero)
; - HL: [Output] Pointer into 8085 program code
;
; The assembled program code requires two parameters before being executed:
; - DE: Pointer to text output memory
; - HL: Pointer to Brainfuck memory
bfasm:
	; Dispatch assembler handler from brainfuck character
	ldax D
	cpi '>'
	jz bfasm_greater
	cpi '<'
	jz bfasm_lesser
	cpi '+'
	jz bfasm_sum
	cpi '-'
	jz bfasm_minus
	cpi 2Eh ; Dot
	jz bfasm_dot
	cpi 2Ch ; Comma
	jz bfasm_comma
	cpi '['
	jz bfasm_openbracket
	cpi ']'
	jz bfasm_closebracket
	cpi 00h ; End of input
	jz bfasm_end
	jmp bfasm_other

bfasm_greater:
	inx D ; Skip '>' character

	mvi M, 23h ; inx H (increase memory pointer)
	inx H

	jmp bfasm_next

bfasm_lesser:
	inx D ; Skip '<' character

	mvi M, 2Bh ; dcx H (decrease memory pointer)
	inx H

	jmp bfasm_next

bfasm_sum:
	inx D ; Skip '+' character

	mvi M, 34h ; inr M (increase memory content)
	inx H

	jmp bfasm_next

bfasm_minus:
	inx D ; Skip '-' character

	mvi M, 35h ; dcr M (decrease memory content)
	inx H

	jmp bfasm_next

bfasm_dot:
	inx D ; Skip '.' character

	mvi M, 7Eh ; mov A, M (load character from memory)
	inx H
	mvi M, 12h ; stax D (put character to text output)
	inx H
	mvi M, 13h ; inx D (increase text output)
	inx H

	jmp bfasm_next

bfasm_comma:
	hlt ; TODO

bfasm_openbracket:
	inx D ; Skip '[' character

	; Save JIT address of start at the top of stack
	push H

	; Reserve space for loop start
	inx H
	inx H
	inx H
	inx H

	; Assemble subprogram
	call bfasm

	; Restore the pointer before the jump to BC
	pop B

	; Assemble jump end
	mvi M, C3h
	inx H
	mov M, C
	inx H
	mov M, B
	inx H

	; Assemble jump start
	mvi A, 7Eh
	stax B
	inx B

	mvi A, CAh
	stax B
	inx B

	mov A, L
	stax B
	inx B
	
	mov A, H
	stax B
	inx B

	jmp bfasm_next


bfasm_closebracket:
	inx D ; Skip the ']' character
	ret

bfasm_end:
	mvi M, 76h ; hlt (stop program)
	inx H

	ret

bfasm_other:
	inx D ; Skip the character
	jmp bfasm_next

bfasm_next:	
	jmp bfasm                    
