; Sierpinsky carpet through pixel iteration
; Requires 160x100, 256 color grayscale screen

.define
	screenMem A000h
	screenWidth 81
	screenHeight 81
	screenStride 160

.org 100h
	lxi D, screenStride-screenWidth
	lxi H, screenMem

	; Loop in range [0, screenWidth) on C
	mvi C, 0
	loopY:
		; Loop in range [0, screenHeight) on B
		mvi B, 0
		loopX:
			; Paint pixel
			call isSierpinski
			jnz notFilled
			mvi M, FFh
			notFilled:

			; Advance to next pixel
			inx H

			; Check for end of X loop
			inr B
			mov A, B
			cpi screenWidth
			jnz loopX



		; Skip unused pixels until next row
		dad D

		; Check for end of Y loop
		inr C
		mov A, C
		cpi screenHeight
		jnz loopY

	hlt

; Checks if the given pixel is in the Sierpinski carpet
; All registers are kept except PSW
; B = X coord, C = Y coord
; Returns zero flag = filled, else unfilled
isSierpinski:
	push B
	push D
	push H

	loop:
		; Return with zero flag (filled) status if X=Y=0
		mov A, B
		ora C
		jz end

		; Divide B and C by 3, and place
		; D = B/3
		; E = B%3
		; H = C/3
		; L = C%3
		mov A, B
		call div3

		mov D, H
		mov E, L

		mov A, C
		call div3

		; If E=L=1, return without zero flag (unfilled) status
		mov A, E
		ana L
		cpi 1
		jnz noEnd
		add a ; Unset Z flag
		jmp end
		noEnd:

		; Else place new quotients and continue
		mov B, D
		mov C, H
		jmp loop

	end:
	pop H
	pop D
	pop B

	ret

; Divides a positive value in the accumulator by 3
; Places the quotient on H
; Places the remainder on L
div3:
	push PSW

	mvi H, 0

	divLoop:
		sui 3
		jm endDivLoop
		inr H
		jmp divLoop

		endDivLoop:

	adi 3
	mov L, A

	pop PSW
	ret