; Sierpinsky triangle through rule 90 (can also be used with other rules)
; Requires 160x100, 256 color grayscale screen

.define
	rule 90
	; In this program we 'abuse' that the pixels after the end
	; of a row (in the screen memory) are always set to zero
	stackMem 0000h
	screenMem A000h
	screenStride 160
	screenWidth 159
	screenHeight 100

.data 1000h
	initialPattern: ; screenWidth bytes
	db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	db 1
	db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	db 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

.org 100h
	lxi SP, stackMem

	; Copy the initial pattern to the screen
	; Register usage:
	; B = Loop counter (screenWidth to 0)
	; DE = Initial pattern pointer
	; HL = Screen pointer
	lxi D, initialPattern
	lxi H, screenMem
	mvi B, screenWidth

	copyInitialPattern:
		ldax D
		ani 1b
		jz noSetIni
		mvi M, FFh
		noSetIni:

		inx D
		inx H

		dcr B
		jnz copyInitialPattern

	; Evolve the system
	; Register usage:
	; Stack = Outermost loop counter (screenHeight to 0)
	;         Temporary used to make space to use DAD on DE and HL
	; B = Innermost loop counter (screenWidth to 0)
	; C = Neighbor cell status
	; DE = Previous row pointer
	; HL = Current row pointer
	lxi D, screenMem
	lxi H, screenMem+screenStride

	mvi A, screenHeight
	push PSW

	evolveRow:
		mvi B, screenWidth

		; Cell status consists of a 3-bit value
		; B2 = Current left-cell
		; B1 = Current center-cell
		; B0 = Current right-cell

		; Load initial cell status (left-cell=0 and center-cell, the shift will be done later!)
		ldax D
		ani 1b
		mov C, A
		
		evolveCell:
			; Update status with the next right-cell & increase prev. row pointer
			; This will 'overflow' at the end of a row but it doesn't matter
			mov A, C ; C = (C << 1) & 0x7 (discard oldest cell and make room)
			ral
			ani 111b
			mov C, A

			inx D ; Load right-cell
			ldax D
			ani 1b

			jz noUpdate ; Set lowest bit of C if cell active
			inr C
			noUpdate:

			; Apply rule
			; We need to do rule>>C and this is harder than it looks :/
			push B
			mov B, C
			mvi A, rule
			rotRule:
				dcr B
				jm endRot
				rar
				jmp rotRule
			endRot:
			pop B

			ani 1b ; Set the screen depending on the corresponding bit of rule
			jz noSet
			mvi M, FFh
			noSet:
			
			
			; Increase screen pointer
			inx H

			; Check for end of row
			dcr B
			jnz evolveCell

		; Increment the screen pointers DE and HL
		push B
		lxi B, screenStride-screenWidth

		xchg
		dad B
		xchg

		dad B

		pop B

		; Check for end of program
		pop PSW
		dcr A
		push PSW
		jnz evolveRow

	pop PSW ; Clean up stack

	hlt