; Fills whole memory with fibonacci numbers mod 256 (except program)
.org 0h
	lxi B, 0
	lxi D, 1
	lxi SP, 0
	loop:
		; Put fibonacci number to memory
		push B

		; Generate next fibonacci number
		lxi H, 0
		dad B
		dad D

		mov B, D
		mov C, E
		mov D, H
		mov E, L

		; Check for end of memory
		lda canary
		ana a
		jz loop

	nop ; Alignment
	hlt

canary:
		nop