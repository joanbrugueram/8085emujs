; Simple counter
; TRAP interrupt on timer event
; Console on FF output port

.org 100h
	; Set up first character and wait
	mvi A, '0'
	loop: jmp loop
	
.org 24h
	; Output number (ASCII) to console port
	out FFh
	; Increment number (ASCII)
	inr A
	; Check overflow and wrap around
	cpi '9'+1
	rm
	mvi A, '0'
	ret