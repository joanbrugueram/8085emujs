.org 100h
	mvi A, '0'
	sta E000h ; Digit 1
	sta E001h ; Digit 2
	loop: jmp loop

.org 24h
	lxi H, E001h

	inr M        ; Increment digit 2

	mov A, M     ; Check for digit 2 overflow
	cpi '9'+1
	rm

	mvi M, '0'   ; Digit 2 overflow
	dcx H

	inr M        ; Increment digit 1

	mov A, M     ; Check for digit 1 overflow
	cpi '6'
	rm

	mvi M, '0'   ; Digit 1 overflow
	ret