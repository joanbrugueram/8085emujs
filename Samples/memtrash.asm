; Linear congruential generator Xn+1 = 7Xn + 123, seed = 42
; Fills memory except program code
.org 0h
	mvi B, 42
	lxi H, endOfProgram
	loop:
		; Generate the next pseudorandom number
		mvi A, 0
		mvi C, 9
		loopMul:
			add B

			dcr C
			jnz loopMul

		adi 123
		mov B, A

		; Write memory
		mov M, B
		inx H

		; Check for end of memory
		mov A, H
		ora L
		jnz loop

	hlt
		
			

endOfProgram:

