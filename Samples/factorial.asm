; Factorial from 0! to 8!
; Keyboard on 00h port
; Text screen on E000h
; TRAP on key press

.org 100h
	; Wait for TRAP interrupt
	loop: jmp loop
	hlt

.org 24h
	; Read the number from the keyboard
	in 00h

	; Check if it's between '0' and '8'
	cpi '0'
	rm
	cpi '9'
	rp

	; Print 'n!=' to the text screen
	lxi H, E000h
	mov M, A
	inx H
	mvi M, '!'
	inx H
	mvi M, '='

	; Convert ASCII->binary
	sui '0'

	; Compute factorial (result at HL)
	call factorial

	; Print the result
	lxi D, E007h ; Text screen pointer
	push H ; Save first quotient
	loopDigit:
		; Emit digit
		pop H ; Recover quotient
		push D ; Save storage address (divHL10 destroys it)
		call divHL10 ; Quotient=BC Remainder=HL
		pop D ; Recover storage address
		push B ; Save new quotient
		mov A, L ; Take remainder
		adi '0' ; Convert binary->ASCII
		stax D ; Put to the text screen

		; Update screen address
		dcx D

		; Check end of loop
		mov A, E
		cpi 2
		jnz loopDigit

	pop H ; Throw last quotient (which should be zero)

	ret

; Computes the factorial of the accumulator and puts it to HL
; Destroys registers A, B, D, E, H, L
factorial:
	lxi H, 1
	loopFactorial:
		; Check end of loop
		dcr a
		jm fiFactorial

		; Multiply HL by the accumulator
		mov D, H
		mov E, L
		lxi H, 0

		mov B, A
		loopMult:
			dad D

			dcr A
			jp loopMult
		mov A, B

		jmp loopFactorial

	fiFactorial:
		ret

; Divides HL by 10
; The quotient goes to BC and the remainder to HL
; Destroys registers A, B, C, D, E, H, L
divHL10:
	lxi B, 0
	lxi D, -10

	loopDiv:
		; Return if HL >= 0 && HL < 10
		mov A, H
		ana A
		jnz procesDiv

		mov A, L
		ana A
		jm procesDiv
		cpi 10
		jp procesDiv
		ret

	procesDiv:
		dad D ; Decrease remainder
		inx B ; Increase quotient

		jmp loopDiv