; 8085 Hello World for text screen
; Requires text screen on 0xE000

.data 200h
	helloWorldString:
		ds "Hello, World!"
		db 0

.org 100h
	lxi D, helloWorldString ; String pointer
	lxi H, E000h           ; Text screen pointer

	loop:
		ldax D          ; Load string character

		ana A
		jz endLoop      ; Exit on NUL terminator

		mov M, A        ; Put character to text screen

		inx D           ; Increment string and text screen pointers
		inx H

		jmp loop
	
	endLoop:
		hlt