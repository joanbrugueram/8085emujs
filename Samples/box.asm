; Box fractal
; Requires 160x100, 256 color grayscale screen

.define
	stackMem 0000h
	screenMem A000h
	screenStride 160
	depth 4

; Register usage:
; SP = Stack pointer
; HL = Screen pointer
; A = Fractal depth
; BC = Vertical step for each depth level
; DE = Horizontal step for each depth level
.org 100h
	lxi SP, stackMem
	lxi H, screenMem
	lxi B, screenStride
	lxi D, 1
	xra A
	call drawBox
	hlt

drawBox:
	cpi depth
	jnz depthNotFinal

	depthFinal:
		mvi M, FFh
		ret

	depthNotFinal:
		; Save regs
		push B
		push D
		push H

		; Multiply the step by 3 (unless on the first level)
		ana A
		jz onFirstLevel
		push H

		lxi H, 0 ; Multiply DE by 3
		dad D
		dad D
		dad D
		mov E, L
		mov D, H

		lxi H, 0 ; Multiply BC by 3
		dad B
		dad B
		dad B
		mov C, L
		mov B, H

		pop H
		onFirstLevel:

		; Increase depth level
		inr A

		; Do the recursive calls (a bit messy, but easiest way)
		push H

		call drawBox ; At (0, 0)

		dad D
		dad D

		call drawBox ; At (2, 0)

		pop H
		dad B
		push H
		dad D

		call drawBox ; At (0, 1)

		pop H
		dad B

		call drawBox ; At (0, 2)

		dad D
		dad D

		call drawBox ; At (2, 2)

		; Restore depth level
		dcr A

		; Restore regs
		pop H
		pop D
		pop B

		ret