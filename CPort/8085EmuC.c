﻿/**************************************
 * JAVASCRIPT 8085 SIMULATOR - C PORT *
 **************************************
 * By joanbrugueram.
 *
 * The architecture is inspired by a 8085 simulator by Angel Manuel Gómez García et al.
 *
 * First public release on 20th April 2013, under the WTFPL license.
 * C port finished on 6th June 2013, also under the WTFPL license.
 */
#include "8085EmuC.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#define ARRAY_SIZE(arr) (sizeof((arr))/sizeof((arr)[0]))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define SWAP(x, y, T) do { T temp = x; x = y; y = temp; } while (0)

static bool parseHexByte(const char *str, uint8_t *val) {
	if (strlen(str) < 2)
		return false;

	for (size_t i = 0; i < 2; i++) {
		*val <<= 4;
		if (str[i] >= '0' && str[i] <= '9')
			*val |= (str[i] - '0');
		else if (str[i] >= 'a' && str[i] <= 'f')
			*val |= (str[i] - 'a' + 10);
		else if (str[i] >= 'A' && str[i] <= 'F')
			*val |= (str[i] - 'A' + 10);
		else
			return false;
	}

	return true;
}

/* Initialize an empty EventQueue. */
static void EventQueue_init(EventQueue *eq) {
	eq->nEvents = 0;
}

/* Check if the EventQueue is empty. */
static bool EventQueue_isEmpty(EventQueue *eq) {
	return eq->nEvents == 0;
}

/* Append an element to the EventQueue. */
static void EventQueue_Push(EventQueue *eq, uint64_t time, Device *device) {
	if (eq->nEvents >= ARRAY_SIZE(eq->events)) {
		fprintf(stderr, "EventQueue_Push OVERFLOW (increase eq->events size?)\n");
		abort();
	}

	struct EventQueue_Event event = { time, device };
	eq->events[eq->nEvents++] = event;

	// "Bubble up"
	for (size_t i = eq->nEvents-1; i > 0; ) {
		if (eq->events[(i-1)/2].time > eq->events[i].time) {
			SWAP(eq->events[(i-1)/2], eq->events[i], struct EventQueue_Event);
			i = (i-1)/2; // Advance to parent
		}
		else
			break;
	}
}

/* Get the priority of element on the EventQueue with the highest priority (lowest time). */
static uint64_t EventQueue_topPriority(EventQueue *eq) {
	if (eq->nEvents == 0) {
		fprintf(stderr, "EventQueue_topPriority UNDERFLOW (check empty?)\n");
		abort();
	}

	return eq->events[0].time;
}

/* Get the value of the element on the EventQueue with the highest priority (lowest time) and remove it. */
static Device *EventQueue_pop(EventQueue *eq) {
	if (eq->nEvents == 0) {
		fprintf(stderr, "EventQueue_pop UNDERFLOW (check empty?)\n");
		abort();
	}

	struct EventQueue_Event ev = eq->events[0];

	// "Sift down"
	eq->events[0] = eq->events[--eq->nEvents];
	for (size_t i = 0; 2*i+1 < eq->nEvents; ) {
		uint64_t lchild = eq->events[2*i+1].time;
		uint64_t rchild = (2*i+2 < eq->nEvents) ? eq->events[2*i+2].time : (uint64_t)-1;
		uint64_t minchild = MIN(lchild, rchild);
		if (eq->events[i].time > minchild) {
			if (minchild == lchild) {
				SWAP(eq->events[i], eq->events[2*i+1], struct EventQueue_Event);
				i = 2*i+1; // Advance to left child
			} else if (minchild == rchild) { // (all other cases)
				SWAP(eq->events[i], eq->events[2*i+2], struct EventQueue_Event);
				i = 2*i+2; // Advance to right child
			}
		}
		else
			break;
	}

	return ev.device;
}

/* Bits of the 8085 flags register. */
typedef enum FLAGBITS {
	FLAGBITS_SIGN = (1 << 7),
	FLAGBITS_ZERO = (1 << 6),
	FLAGBITS_AUXCARRY = (1 << 4),
	FLAGBITS_PARITY = (1 << 2),
	FLAGBITS_CARRY = (1 << 0)
} FLAGBITS;

/* Reset the machine to its initial state. */
void Machine_reset(Machine *m) {
	#if 1
	// Self-test
	m->a = m->b = m->d = m->h = 0x12;
	m->f = m->c = m->e = m->l = 0x34;
	if (m->psw != 0x1234 || m->bc != 0x1234 || m->de != 0x1234 || m->hl != 0x1234)
		fprintf(stderr, "Machine_reset: Register union problem (endianness?)\n");
	#endif

	m->psw = 0x0000;
	m->bc = 0x0000;
	m->de = 0x0000;
	m->hl = 0x0000;
	m->sp = 0x0000;
	m->pc = 0x0000;

	m->intrEnable = false;
	m->intrMask55 = false;
	m->intrMask65 = false;
	m->intrMask75 = false;
	m->intrPend55 = false;
	m->intrPend65 = false;
	m->intrPend75 = false;
	m->intrPendTrap = false;

	m->serialIn = false;
	m->serialOutEnabled = false;
	m->serialOut = false;

	memset(m->memory, 0, sizeof(m->memory));
	memset(m->inp, 0, sizeof(m->inp));
	memset(m->outp, 0, sizeof(m->outp));

	EventQueue_init(&m->eventQueue);
	m->timer = 0;

	m->debugMode = false;
}

/* Attach a device to this machine. */
void Machine_attachDevice(Machine *m, Device *device) {
	if (m->nDevices >= ARRAY_SIZE(m->devices)) {
		fprintf(stderr, "Machine_attachDevice OVERFLOW (increase m->devices size?)\n");
		abort();
	}

	// Add the device to the device list
	m->devices[m->nDevices++] = device;

	// Call the device attach callback
	device->onAttach(device, m);
}

/* Load a program from an object file */
/* FIXME: Take a "const char *" and don't trash the string. */
bool Machine_loadProgram(Machine *m, char *program) {
	const char *whitespace = " \t\n\v\f\r";

	uint16_t currPos = 0;
	bool pcDefined = false;
	for (char *tok = strtok(program, whitespace); tok != NULL; tok = strtok(NULL, whitespace)) {
		if (strcmp(tok, ".ORG") == 0 || strcmp(tok, ".DATA") == 0) {
			char *word1, *word2;
			uint8_t lo, hi;
			if ((word1 = strtok(NULL, whitespace)) == NULL || !parseHexByte(word1, &lo))
				return false;
			if ((word2 = strtok(NULL, whitespace)) == NULL || !parseHexByte(word2, &hi))
				return false;

			currPos = (hi << 8) | lo;

			if (strcmp(tok, ".ORG") == 0 && !pcDefined) {
				m->pc = currPos;
				pcDefined = true;
			}
		} else {
			uint8_t val;
			if (!parseHexByte(tok, &val))
				return false;
			Machine_writeMemory8(m, currPos++, val);
		}
	}

	m->intrEnable = true; // This is enabled by default in the simulator
	return true;
}

/* Read a 8-bit value from the memory. */
uint8_t Machine_readMemory8(const Machine *m, uint16_t addr) {
	return m->memory[addr % sizeof(m->memory)];
}

/* Write a 8-bit value to the memory. */
void Machine_writeMemory8(Machine *m, uint16_t addr, uint8_t value) {
	m->memory[addr % sizeof(m->memory)] = value;
}

/* Read a 16-bit value from the specified memory address.
 * The first byte is the low byte and the second byte is the high byte. */
uint16_t Machine_readMemory16(const Machine *m, uint16_t addr) {
	uint16_t value = m->memory[addr % sizeof(m->memory)];
	value |= m->memory[(addr+1) % sizeof(m->memory)] << 8;
	return value;
}

/* Write a 16-bit value to the specified memory address.
 * The first byte is the low byte and the second byte is the high byte. */
void Machine_writeMemory16(Machine *m, uint16_t addr, uint16_t value) {
	m->memory[addr % sizeof(m->memory)] = value & 0xFF;
	m->memory[(addr+1) % sizeof(m->memory)] = value >> 8;
}

/* Set the specified input port to the specified value. */
void Machine_setInPort(Machine *m, uint8_t port, uint8_t value) {
	m->inp[port] = value;
}

/* Get the value of the specified output port. */
uint8_t Machine_getOutPort(Machine *m, uint8_t port) {
	return m->outp[port];	
}

/* Requests an interrupt to happen (sets the pending bits). 
 * (It may or may not happen depending on the masks.) */
void Machine_requestInterrupt(Machine *m, INTERRUPTTYPE type) {
	switch (type) {
		case INTERRUPTTYPE_TRAP: m->intrPendTrap = true; break;
		case INTERRUPTTYPE_RST75: m->intrPend75 = true; break;
		case INTERRUPTTYPE_RST65: m->intrPend65 = true; break;
		case INTERRUPTTYPE_RST55: m->intrPend55 = true; break;
	}
}

/* Adds an event from the specified device, which will happen at the specified cycle. */
void Machine_addEvent(Machine *m, uint64_t time, Device *device) {
	EventQueue_Push(&m->eventQueue, time, device);
}

/* Gets the value of one of the flags on the flags register. */
static bool Machine_getFlag(const Machine *m, FLAGBITS flag) {
	return (m->f & flag) != 0;
}


/* Sets the value of one of the flags on the flags register. */
static void Machine_setFlag(Machine *m, FLAGBITS flag, bool val) {
	m->f = (m->f & ~flag) | (val ? flag : 0);
}

static const uint8_t parityTable[256] = { // XOR of all bits of integers from 0 to 255
	0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
	1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
	1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
	0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
	1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0,
	0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
	0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
	1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0
};

/* Set the sign, zero and parity flags depending on a 8-bit value. */
static void Machine_setFlagsSZP(Machine *m, uint8_t val) {
	Machine_setFlag(m, FLAGBITS_SIGN, (val & 0x80) != 0);
	Machine_setFlag(m, FLAGBITS_ZERO, val == 0);
	Machine_setFlag(m, FLAGBITS_PARITY, parityTable[val] == 0);
	// Note that the parity bit is set if the parity is 0 (even number of bits),
	// and unset if the parity is 1 (odd number of bits), this is somewhat unusual!
}

/* Adds the specified value to the accumulator and sets the corresponding flags.
 * If carrybit is set to 1, an extra unit is added to the result. */
static void Machine_addToAccumulator(Machine *m, uint8_t val, bool carrybit) {
	int realval = val + (carrybit ? 1 : 0);
	Machine_setFlag(m, FLAGBITS_CARRY, (m->a + realval) >= 0x100);
	Machine_setFlag(m, FLAGBITS_AUXCARRY, ((m->a & 0xF) + (realval & 0xF)) >= 0x10);
	m->a += realval;
	Machine_setFlagsSZP(m, m->a);
}

/* Substracts the specified value to the accumulator and sets the corresponding flags.
 * If carrybit is set to 1, an extra unit is substracted to the result. */
static void Machine_subToAccumulator(Machine *m, uint8_t val, bool carrybit) {
	// Usually, this is two's complement.
	// However, this will act diferently on instrs. like SBB/SBI
	Machine_addToAccumulator(m, (uint8_t)~val, !carrybit);

	// Carry bit is borrow bit
	Machine_setFlag(m, FLAGBITS_CARRY, !Machine_getFlag(m, FLAGBITS_CARRY));
}

/* ANDs the specified value to the accumulator and sets the corresponding flags. */
static void Machine_andAccumulator(Machine *m, uint8_t val) {
	Machine_setFlag(m, FLAGBITS_CARRY, false);
	Machine_setFlag(m, FLAGBITS_AUXCARRY, true);
	m->a &= val;
	Machine_setFlagsSZP(m, m->a);
}

/* ORs the specified value to the accumulator and sets the corresponding flags. */
static void Machine_orAccumulator(Machine *m, uint8_t val) {
	Machine_setFlag(m, FLAGBITS_CARRY, false);
	Machine_setFlag(m, FLAGBITS_AUXCARRY, false);
	m->a |= val;
	Machine_setFlagsSZP(m, m->a);
}

/* XORs the specified value to the accumulator and sets the corresponding flags. */
static void Machine_xorAccumulator(Machine *m, uint8_t val) {
	Machine_setFlag(m, FLAGBITS_CARRY, false);
	Machine_setFlag(m, FLAGBITS_AUXCARRY, false);
	m->a ^= val;
	Machine_setFlagsSZP(m, m->a);
}

/* Push a 16-bit value to the stack. */
static void Machine_pushValue(Machine *m, uint16_t value) {
	m->sp -= 2;
	Machine_writeMemory16(m, m->sp, value);
}

/* Pop a 16-bit value from the stack. */
static uint16_t Machine_popValue(Machine *m) {
	uint16_t value = Machine_readMemory16(m, m->sp);
	m->sp += 2;
	return value;
}

/* Does a subroutine call. */
static void Machine_callTo(Machine *m, uint16_t addr) {
	Machine_pushValue(m, m->pc);
	m->pc = addr;
}


static void Machine_jumpToInterruptVector(Machine *m, uint16_t addr) {
	// Disable handling other interrupts (so the interrupt priority can work)
	m->intrEnable = false;

	// Call the interrupt handler
	Machine_callTo(m, addr);
}

/* Addressable 8 bit items */
typedef enum REG8 {
	REG8_B = 0,
	REG8_C = 1,
	REG8_D = 2,
	REG8_E = 3,
	REG8_H = 4,
	REG8_L = 5,
	REG8_M = 6, // Not really a register
	REG8_A = 7
} REG8;

/* Addressable 16 bit items */
typedef enum REG16 {
	REG16_BC = 0,
	REG16_DE = 1,
	REG16_HL = 2,
	REG16_PSW = 3,
	REG16_SP = 3 // Not a mistake; depends on instruction
} REG16;

/* Jump contions */
typedef enum CONDITION {
	CONDITION_IfNotZero = 0,
	CONDITION_IfZero = 1,
	CONDITION_IfNotCarry = 2,
	CONDITION_IfCarry = 3,
	CONDITION_IfNotParity = 4,
	CONDITION_IfParity = 5,
	CONDITION_IfNotSign = 6,
	CONDITION_IfSign = 7
} CONDITION;

/* INTERNAL operation codes (generated by the decode method) */
typedef enum OPCODES {
	/* -------------
	 * DATA TRANSFER
	 * -------------*/
	// MOV DST8, SRC8
	OPCODES_MOV = 0,

	// MVI DST8, IMM8
	OPCODES_MVI = 1,
	// LXI DST16, IMM16
	OPCODES_LXI = 2,

	// LDA IMM16
	OPCODES_LDA = 3,
	// STA IMM16
	OPCODES_STA = 4,

	// LHLD IMM16
	OPCODES_LHLD = 5,
	// SHLD IMM16
	OPCODES_SHLD = 6,

	// LDAX SRC16 (WARNING: SRC16 is limited to BC, DE)
	OPCODES_LDAX = 7,
	// STAX DST16 (WARNING: SRC16 is limited to BC, DE)
	OPCODES_STAX = 8,

	// XCHG
	OPCODES_XCHG = 9,
	// SPHL
	OPCODES_SPHL = 10,
	// XTHL
	OPCODES_XTHL = 11,

	// PUSH SRC16 (WARNING: SRC16 is limited to BC, DE, HL, PSW)
	OPCODES_PUSH = 12,
	// POP DST16 (WARNING: DST16 is limited to BC, DE, HL, PSW)
	OPCODES_POP = 13,

	// IN IMM8
	OPCODES_IN = 14,
	// OUT IMM8
	OPCODES_OUT = 15,

	/* ----------
	 * ARITHMETIC
	 * ----------*/
	// ADD SRC8
	OPCODES_ADD = 16,
	// ADI IMM8
	OPCODES_ADI = 17,

	// ADC SRC8
	OPCODES_ADC = 18,
	// ACI IMM8
	OPCODES_ACI = 19,

	// SUB SRC8
	OPCODES_SUB = 20,
	// SUI IMM8
	OPCODES_SUI = 21,

	// SBB SRC8
	OPCODES_SBB = 22,
	// SBI SRC8
	OPCODES_SBI = 23,

	// INR DST8
	OPCODES_INR = 24,
	// DCR DST8
	OPCODES_DCR = 25,

	// INX DST16
	OPCODES_INX = 26,
	// DCX DST16
	OPCODES_DCX = 27,

	// DAD SRC16
	OPCODES_DAD = 28,

	// DAA
	OPCODES_DAA = 29,

	/* ---------
	 * BRANCHING
	 * ---------*/
	// JMP IMM16
	OPCODES_JMP = 30,
	// JX COND IMM16
	OPCODES_JX = 31,

	// CALL IMM16
	OPCODES_CALL = 32,
	// CX COND IMM16
	OPCODES_CX = 33,

	// RET
	OPCODES_RET = 34,
	// RX COND
	OPCODES_RX = 35,

	// PCHL
	OPCODES_PCHL = 36,

	// RST IMM8 (Not really a IMM8 but restart ID will be saved to IMM8 field)
	OPCODES_RST = 37,

	/* -------
	 * LOGICAL
	 * -------*/
	// CMP REG8
	OPCODES_CMP = 38,
	// CPI IMM88
	OPCODES_CPI = 39,

	// ANA REG8
	OPCODES_ANA = 40,
	// ANI IMM8
	OPCODES_ANI = 41,

	// ORA REG8
	OPCODES_ORA = 42,
	// ORI IMM8
	OPCODES_ORI = 43,

	// XRA REG8
	OPCODES_XRA = 44,
	// XRI IMM8
	OPCODES_XRI = 45,

	// RLC
	OPCODES_RLC = 46,
	// RRC
	OPCODES_RRC = 47,

	// RAL
	OPCODES_RAL = 48,
	// RAR
	OPCODES_RAR = 49,

	// CMA
	OPCODES_CMA = 50,

	// CMC
	OPCODES_CMC = 51,
	// STC
	OPCODES_STC = 52,

	/* -------
	 * CONTROL
	 * -------*/
	// NOP
	OPCODES_NOP = 53,

	// HLT
	OPCODES_HLT = 54,

	// DI
	OPCODES_DI = 55,
	// EI
	OPCODES_EI = 56,

	// RIM
	OPCODES_RIM = 57,
	// SIM
	OPCODES_SIM = 58,

	// UNKNOWN/INVALID OPCODE (The opcode is saved in the IMM8 field)
	OPCODES_UNKNOWN = 59
} OPCODES;

static uint8_t *Machine_getReg8(Machine *m, REG8 reg8) {
	switch (reg8) {
		case REG8_B: return &m->b;
		case REG8_C: return &m->c;
		case REG8_D: return &m->d;
		case REG8_E: return &m->e;
		case REG8_H: return &m->h;
		case REG8_L: return &m->l;
		case REG8_M: return &m->memory[m->hl % sizeof(m->memory)];
		case REG8_A: return &m->a;
		default:     return NULL; // Should not happen!
	}
}

static uint16_t *Machine_getReg16_1(Machine *m, REG16 reg16) {
	switch (reg16) {
		case REG16_BC: return &m->bc;
		case REG16_DE: return &m->de;
		case REG16_HL: return &m->hl;
		case REG16_SP: return &m->sp;
		default:       return NULL; // Should not happen!
	}
}

static uint16_t *Machine_getReg16_2(Machine *m, REG16 reg16) {
	switch (reg16) {
		case REG16_BC:  return &m->bc;
		case REG16_DE:  return &m->de;
		case REG16_HL:  return &m->hl;
		case REG16_PSW: return &m->psw;
		default:        return NULL; // Should not happen!
	}
}

static bool Machine_evalCondition(const Machine *m, CONDITION cond) {
	switch (cond) {
		case CONDITION_IfNotZero:   return !Machine_getFlag(m, FLAGBITS_ZERO);
		case CONDITION_IfZero:      return Machine_getFlag(m, FLAGBITS_ZERO);
		case CONDITION_IfNotCarry:  return !Machine_getFlag(m, FLAGBITS_CARRY);
		case CONDITION_IfCarry:     return Machine_getFlag(m, FLAGBITS_CARRY);
		case CONDITION_IfNotParity: return !Machine_getFlag(m, FLAGBITS_PARITY);
		case CONDITION_IfParity:    return Machine_getFlag(m, FLAGBITS_PARITY);
		case CONDITION_IfNotSign:   return !Machine_getFlag(m, FLAGBITS_SIGN);
		case CONDITION_IfSign:      return Machine_getFlag(m, FLAGBITS_SIGN);
		default:                    return false; // Should not happen!
	}
}

typedef struct Instruction { // TODO optimize
	OPCODES opcode;
	REG8 src8;
	REG8 dst8;
	REG16 src16;
	REG16 dst16;
	CONDITION condition;
	uint8_t imm8;
	uint16_t imm16;
} Instruction;

/* Fetches the instruction at the current program counter and generates a instruction structure,
 * which can then be executed (details about it can be found on the OPCODE declaration). */
static Instruction Machine_decode(Machine *m) {
	Instruction instr;

	#define SETOPCODE(op) instr.opcode = op;
	#define SETSRC8(bit) instr.src8 = (REG8)((Machine_readMemory8(m, m->pc-1) >> bit) & 0x7);
	#define SETDST8(bit) instr.dst8 = (REG8)((Machine_readMemory8(m, m->pc-1) >> bit) & 0x7);
	#define SETSRC16(bit) instr.src16 = (REG16)((Machine_readMemory8(m, m->pc-1) >> bit) & 0x3);
	#define SETDST16(bit) instr.dst16 = (REG16)((Machine_readMemory8(m, m->pc-1) >> bit) & 0x3);
	#define SETCONDITION(bit) instr.condition = (CONDITION)((Machine_readMemory8(m, m->pc-1) >> bit) & 0x7);
	#define SETIMM8() instr.imm8 = Machine_readMemory8(m, m->pc); m->pc += 1;
	#define SETIMM16() instr.imm16 = Machine_readMemory16(m, m->pc); m->pc += 2;

	switch (Machine_readMemory8(m, m->pc++)) {
		// MOV DST8, SRC8 instructions: 01 ddd sss format
		// The situation DST8 = SRC8 = M is special (0x76=HLT instruction)
	case 0x40: case 0x48: case 0x50: case 0x58: case 0x60: case 0x68: case 0x70: case 0x78:
	case 0x41: case 0x49: case 0x51: case 0x59: case 0x61: case 0x69: case 0x71: case 0x79:
	case 0x42: case 0x4A: case 0x52: case 0x5A: case 0x62: case 0x6A: case 0x72: case 0x7A:
	case 0x43: case 0x4B: case 0x53: case 0x5B: case 0x63: case 0x6B: case 0x73: case 0x7B:
	case 0x44: case 0x4C: case 0x54: case 0x5C: case 0x64: case 0x6C: case 0x74: case 0x7C:
	case 0x45: case 0x4D: case 0x55: case 0x5D: case 0x65: case 0x6D: case 0x75: case 0x7D:
	case 0x46: case 0x4E: case 0x56: case 0x5E: case 0x66: case 0x6E:            case 0x7E:
	case 0x47: case 0x4F: case 0x57: case 0x5F: case 0x67: case 0x6F: case 0x77: case 0x7F: 
		SETOPCODE(OPCODES_MOV); SETDST8(3); SETSRC8(0); break;
		// MVI DST8, IMM8 instructions: 00 ddd 110 format
	case 0x06: case 0x0E: case 0x16: case 0x1E: case 0x26: case 0x2E: case 0x36: case 0x3E: 
		SETOPCODE(OPCODES_MVI); SETDST8(3); SETIMM8(); break;
		// LXI DST16, IMM16 instructions: 00 dd 0001 format
	case 0x01: case 0x11: case 0x21: case 0x31:
		SETOPCODE(OPCODES_LXI); SETDST16(4); SETIMM16(); break;
		// LDA IMM16 instructions: 00 111 010
	case 0x3A: SETOPCODE(OPCODES_LDA); SETIMM16(); break;
		// STA IMM16 instruction: 00 110 010
	case 0x32: SETOPCODE(OPCODES_STA); SETIMM16(); break;
		// LHDL IMM16 instruction: 00 101 010
	case 0x2A: SETOPCODE(OPCODES_LHLD); SETIMM16(); break;
		// SHDL IMM16 instruction: 00 100 010
	case 0x22: SETOPCODE(OPCODES_SHLD); SETIMM16(); break;
		// LDAX SRC16 instructions: 000 s 1010
	case 0x0A: case 0x1A:
		SETOPCODE(OPCODES_LDAX); SETSRC16(4); break; // Tricky SETSRC16!
		// STAX DST16 instructions: 000 s 0010
	case 0x02: case 0x12:
		SETOPCODE(OPCODES_STAX); SETSRC16(4); break; // Tricky SETSRC16!
		// XCHG instruction: 11 101 011
	case 0xEB: SETOPCODE(OPCODES_XCHG); break;
		// SPHL instruction: 11 111 001
	case 0xF9: SETOPCODE(OPCODES_SPHL); break;
		// XTHL instruction: 11 100 011
	case 0xE3: SETOPCODE(OPCODES_XTHL); break;
		// PUSH SRC16 instruction: 11 ss 0101
	case 0xC5: case 0xD5: case 0xE5: case 0xF5:
		SETOPCODE(OPCODES_PUSH); SETSRC16(4); break;
		// POP DST16 instruction: 11 dd 0001
	case 0xC1: case 0xD1: case 0xE1: case 0xF1:
		SETOPCODE(OPCODES_POP); SETDST16(4); break;
		// IN IMM8 instruction: 11 011 011
	case 0xDB: SETOPCODE(OPCODES_IN); SETIMM8(); break;
		// OUT IMM8 instruction: 11 010 011
	case 0xD3: SETOPCODE(OPCODES_OUT); SETIMM8(); break;


		// ADD SRC8 instructions: 10 000 sss format
	case 0x80: case 0x81: case 0x82: case 0x83: case 0x84: case 0x85: case 0x86: case 0x87:
		SETOPCODE(OPCODES_ADD); SETSRC8(0); break;
		// ADI IMM8 instruction: 11 000 110
	case 0xC6: SETOPCODE(OPCODES_ADI); SETIMM8(); break;
		// ADC SRC8 instructions: 10 001 sss
	case 0x88: case 0x89: case 0x8A: case 0x8B: case 0x8C: case 0x8D: case 0x8E: case 0x8F:
		SETOPCODE(OPCODES_ADC); SETSRC8(0); break;
		// ACI IMM8 instruction: 11 001 110
	case 0xCE:
		SETOPCODE(OPCODES_ACI); SETIMM8(); break;
		// SUB SRC8 instructions: 10 010 sss format
	case 0x90: case 0x91: case 0x92: case 0x93: case 0x94: case 0x95: case 0x96: case 0x97:
		SETOPCODE(OPCODES_SUB); SETSRC8(0); break;
		// SUI IMM8 instruction: 11 010 110
	case 0xD6: SETOPCODE(OPCODES_SUI); SETIMM8(); break;
		// SBB SRC8 instructions: 10 011 sss format
	case 0x98: case 0x99: case 0x9A: case 0x9B: case 0x9C: case 0x9D: case 0x9E: case 0x9F:
		SETOPCODE(OPCODES_SBB); SETSRC8(0); break;
		// SBI IMM8 instruction: 11 011 110
	case 0xDE: SETOPCODE(OPCODES_SBI); SETIMM8(); break;
		// INR DST8 instructions: 00 ddd 100 format
	case 0x04: case 0x0C: case 0x14: case 0x1C: case 0x24: case 0x2C: case 0x34: case 0x3C:
		SETOPCODE(OPCODES_INR); SETDST8(3); break;
		// DCR DST8 instructions: 00 ddd 101 format
	case 0x05: case 0x0D: case 0x15: case 0x1D: case 0x25: case 0x2D: case 0x35: case 0x3D:
		SETOPCODE(OPCODES_DCR); SETDST8(3); break;
		// INX DST16 instruction: 00 dd 0011
	case 0x03: case 0x13: case 0x23: case 0x33:
		SETOPCODE(OPCODES_INX); SETDST16(4); break;
		// DCX DST16 instruction: 00 dd 1011
	case 0x0B: case 0x1B: case 0x2B: case 0x3B:
		SETOPCODE(OPCODES_DCX); SETDST16(4); break;
		// DAD SRC16 instructions: 00 ss 1001 format
	case 0x09: case 0x19: case 0x29: case 0x39:
		SETOPCODE(OPCODES_DAD); SETSRC16(4); break;
		// DAA instruction: 00 100 111
	case 0x27: SETOPCODE(OPCODES_DAA); break;


		// Inconditional JMP IMM16 instruction: 11 000 011
	case 0xC3: SETOPCODE(OPCODES_JMP); SETIMM16(); break;
		// Conditional JMP IMM16 instructions: 11 ccc 010
	case 0xC2: case 0xCA: case 0xD2: case 0xDA: case 0xE2: case 0xEA: case 0xF2: case 0xFA:
		SETOPCODE(OPCODES_JX); SETCONDITION(3); SETIMM16(); break;
		// Inconditional CALL IMM16 instruction: 11 001 101
	case 0xCD: SETOPCODE(OPCODES_CALL); SETIMM16(); break;
		// Conditional CALL IMM16 instructions: 11 ccc 010
	case 0xC4: case 0xCC: case 0xD4: case 0xDC: case 0xE4: case 0xEC: case 0xF4: case 0xFC:
		SETOPCODE(OPCODES_CX); SETCONDITION(3); SETIMM16(); break;
		// Inconditional RET instruction: 11 001 001
	case 0xC9: SETOPCODE(OPCODES_RET); break;
		// Conditional RET instructions: 11 ccc 000
	case 0xC0: case 0xC8: case 0xD0: case 0xD8: case 0xE0: case 0xE8: case 0xF0: case 0xF8:
		SETOPCODE(OPCODES_RX); SETCONDITION(3); break;
		// PCHL instruction: 11 101 001
	case 0xE9: SETOPCODE(OPCODES_PCHL); break;
		// RST instruction: 11 sss 111
	case 0xC7: case 0xCF: case 0xD7: case 0xDF: case 0xE7: case 0xEF: case 0xF7: case 0xFF:
		SETOPCODE(OPCODES_RST);
		instr.imm8 = (Machine_readMemory8(m, m->pc-1) >> 3) & 0x7;
		break;


		// CMP SRC8 instructions: 10 111 sss format
	case 0xB8: case 0xB9: case 0xBA: case 0xBB: case 0xBC: case 0xBD: case 0xBE: case 0xBF:
		SETOPCODE(OPCODES_CMP); SETSRC8(0); break;
		// CPI IMM8 instructions: 11 111 110
	case 0xFE: SETOPCODE(OPCODES_CPI); SETIMM8(); break;
		// ANA SRC8 instructions: 10 100 sss
	case 0xA0: case 0xA1: case 0xA2: case 0xA3: case 0xA4: case 0xA5: case 0xA6: case 0xA7:
		SETOPCODE(OPCODES_ANA); SETSRC8(0); break;
		// ANI IMM8 instructions: 11 100 110 format
	case 0xE6:
		SETOPCODE(OPCODES_ANI); SETIMM8(); break;
		// ORA SRC8 instructions: 10 110 sss format
	case 0xB0: case 0xB1: case 0xB2: case 0xB3: case 0xB4: case 0xB5: case 0xB6: case 0xB7:
		SETOPCODE(OPCODES_ORA); SETSRC8(0); break;
		// ORI IMM8 instructions: 11 110 110 format
	case 0xF6:
		SETOPCODE(OPCODES_ORI); SETIMM8(); break;
		// XRA SRC8 instruction: 10 101 sss
	case 0xA8: case 0xA9: case 0xAA: case 0xAB: case 0xAC: case 0xAD: case 0xAE: case 0xAF:
		SETOPCODE(OPCODES_XRA); SETSRC8(0); break;
		// XRI IMM8 instruction: 11 101 110 format
	case 0xEE:
		SETOPCODE(OPCODES_XRI); SETIMM8(); break;
		// RLC instruction: 00 000 111
	case 0x07: SETOPCODE(OPCODES_RLC); break;
		// RRC instruction: 00 001 111
	case 0x0F: SETOPCODE(OPCODES_RRC); break;
		// RAL instruction: 00 010 111
	case 0x17: SETOPCODE(OPCODES_RAL); break;
		// RAR instruction: 00 011 111
	case 0x1F: SETOPCODE(OPCODES_RAR); break;
		// CMA instruction: 00 101 111
	case 0x2F: SETOPCODE(OPCODES_CMA); break;
		// CMC instruction: 00 111 111
	case 0x3F: SETOPCODE(OPCODES_CMC); break;
		// STC instruction: 00 110 111
	case 0x37: SETOPCODE(OPCODES_STC); break;
		// NOP instruction: 00 000 00
	case 0x00: SETOPCODE(OPCODES_NOP); break;
		// HLT instruction: 01 110 110 format (like what should be "MOV M, M")
	case 0x76: SETOPCODE(OPCODES_HLT); break;
		// DI instruction: 11 110 011
	case 0xF3: SETOPCODE(OPCODES_DI); break;
		// EI instruction: 11 111 011
	case 0xFB: SETOPCODE(OPCODES_EI); break;
		// RIM instruction: 00 100 000
	case 0x20: SETOPCODE(OPCODES_RIM); break;
		// SIM instruction: 00 110 000
	case 0x30: SETOPCODE(OPCODES_SIM); break;

	default:
		SETOPCODE(OPCODES_UNKNOWN);
		instr.imm8 = Machine_readMemory8(m, m->pc-1);
		break;
	}

	return instr;
}

/* Runs a 8085 instruction from an instruction structure (made by decode()). */
static void Machine_runInstruction(Machine *m, Instruction instr) {
	switch (instr.opcode) {
		case OPCODES_MOV: {
			*Machine_getReg8(m, instr.dst8) = *Machine_getReg8(m, instr.src8);
			m->timer += (instr.src8 == REG8_M || instr.dst8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_MVI: {
			*Machine_getReg8(m, instr.dst8) = instr.imm8;
			m->timer += (instr.dst8 == REG8_M) ? 10 : 7;
		} break;
		case OPCODES_LXI: {
			*Machine_getReg16_1(m, instr.dst16) = instr.imm16;
			m->timer += 10;
		} break;
		case OPCODES_LDA: {
			m->a = Machine_readMemory8(m, instr.imm16);
			m->timer += 13;
		} break;
		case OPCODES_STA: {
			Machine_writeMemory8(m, instr.imm16, m->a);
			m->timer += 13;
		} break;
		case OPCODES_LHLD: {
			m->hl = Machine_readMemory16(m, instr.imm16);
			m->timer += 16;
		} break;
		case OPCODES_SHLD: {
			Machine_writeMemory16(m, instr.imm16, m->hl);
			m->timer += 16;
		} break;
		case OPCODES_LDAX: {
			m->a = Machine_readMemory8(m, *Machine_getReg16_1(m, instr.src16));
			m->timer += 7;
		} break;
		case OPCODES_STAX: {
			Machine_writeMemory8(m, *Machine_getReg16_1(m, instr.src16), m->a);
			m->timer += 7;
		} break;
		case OPCODES_XCHG: {
			uint16_t tmp = m->hl;
			m->hl = m->de;
			m->de = tmp;

			m->timer += 4;
		} break;
		case OPCODES_SPHL: {
			m->sp = m->hl;
			m->timer += 6;
		} break;
		case OPCODES_XTHL: {
			uint16_t tmp = Machine_popValue(m);
			Machine_pushValue(m, m->hl);
			m->hl = tmp;

			m->timer += 16;
		} break;
		case OPCODES_PUSH: {
			Machine_pushValue(m, *Machine_getReg16_2(m, instr.src16));
			m->timer += 12;
		} break;
		case OPCODES_POP: {
			*Machine_getReg16_2(m, instr.dst16) = Machine_popValue(m);
			m->timer += 10;
		} break;
		case OPCODES_IN: {
			m->a = m->inp[instr.imm8];
			m->timer += 10;
		} break;
		case OPCODES_OUT: {
			m->outp[instr.imm8] = m->a;

			// Call the devices out callback
			for (size_t i = 0; i < m->nDevices; i++)
				m->devices[i]->onOut(m->devices[i], instr.imm8);

			m->timer += 10;
		} break;


		case OPCODES_ADD: {
			Machine_addToAccumulator(m, *Machine_getReg8(m, instr.src8), false);
			m->timer += (instr.src8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_ADI: {
			Machine_addToAccumulator(m, instr.imm8, false);
			m->timer += 7;
		} break;
		case OPCODES_ADC: {
			Machine_addToAccumulator(m, *Machine_getReg8(m, instr.src8), (m->f & FLAGBITS_CARRY) != 0);
			m->timer += (instr.src8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_ACI: {
			Machine_addToAccumulator(m, instr.imm8, (m->f & FLAGBITS_CARRY) != 0);
			m->timer += 7;
		} break;
		case OPCODES_SUB: {
			Machine_subToAccumulator(m, *Machine_getReg8(m, instr.src8), false);
			m->timer += (instr.src8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_SUI: {
			Machine_subToAccumulator(m, instr.imm8, false);
			m->timer += 7;
		} break;
		case OPCODES_SBB: {
			Machine_subToAccumulator(m, *Machine_getReg8(m, instr.src8), (m->f & FLAGBITS_CARRY) != 0);
			m->timer += (instr.src8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_SBI: {
			Machine_subToAccumulator(m, instr.imm8, (m->f & FLAGBITS_CARRY) != 0);
			m->timer += 7;
		} break;
		case OPCODES_INR: {
			(*Machine_getReg8(m, instr.dst8))++;
			uint8_t val = *Machine_getReg8(m, instr.dst8);
			Machine_setFlag(m, FLAGBITS_AUXCARRY, (val & 0xF) == 0x0);
			Machine_setFlagsSZP(m, val);

			m->timer += (instr.dst8 == REG8_M) ? 10 : 4;
		} break;
		case OPCODES_DCR: {
			(*Machine_getReg8(m, instr.dst8))--;
			uint8_t val = *Machine_getReg8(m, instr.dst8);
			Machine_setFlag(m, FLAGBITS_AUXCARRY, (val & 0xF) == 0xF);
			Machine_setFlagsSZP(m, val);

			m->timer += (instr.dst8 == REG8_M) ? 10 : 4;
		} break;
		case OPCODES_INX: {
			(*Machine_getReg16_1(m, instr.dst16))++;
			m->timer += 6;
		} break;
		case OPCODES_DCX: {
			(*Machine_getReg16_1(m, instr.dst16))--;
			m->timer += 6;
		} break;
		case OPCODES_DAD: {
			uint16_t src = *Machine_getReg16_1(m, instr.src16);
			Machine_setFlag(m, FLAGBITS_CARRY, m->hl + src < m->hl);
			m->hl += src;

			m->timer += 10;
		} break;
		case OPCODES_DAA:
			// Low nibble
			if ((m->a & 0x0F) >= 0x09 || Machine_getFlag(m, FLAGBITS_CARRY)) {
				Machine_setFlag(m, FLAGBITS_AUXCARRY, (m->a & 0x0F) >= 0x09);
				m->a += 0x06;
			} else {
				Machine_setFlag(m, FLAGBITS_AUXCARRY, false);
			}
			// High nibble
			if ((m->a & 0xF0) >= 0x90 || Machine_getFlag(m, FLAGBITS_CARRY)) {
				Machine_setFlag(m, FLAGBITS_CARRY, (m->a & 0xF0) >= 0x90);
				m->a += 0x60;
			} else {
				Machine_setFlag(m, FLAGBITS_CARRY, false);
			}
			Machine_setFlagsSZP(m, m->a);
			m->timer += 4;
			break;


		case OPCODES_JMP: {
			m->pc = instr.imm16;
			m->timer += 10;
		} break;
		case OPCODES_JX: {
			if (Machine_evalCondition(m, instr.condition)) {
				m->pc = instr.imm16;
				m->timer += 10;
			} else {
				m->timer += 7;
			}
		} break;
		case OPCODES_CALL: {
			Machine_callTo(m, instr.imm16);
			m->timer += 18;
		} break;
		case OPCODES_CX: {
			if (Machine_evalCondition(m, instr.condition)) {
				Machine_callTo(m, instr.imm16);
				m->timer += 18;
			} else {
				m->timer += 9;
			}
		} break;
		case OPCODES_RET: {
			m->pc = Machine_popValue(m);
			m->timer += 10;
		} break;
		case OPCODES_RX: {
			if (Machine_evalCondition(m, instr.condition)) {
				m->pc = Machine_popValue(m);
				m->timer += 12;
			} else {
				m->timer += 6;
			}
		} break;
		case OPCODES_PCHL: {
			m->pc = m->hl;
			m->timer += 6;
		} break;
		case OPCODES_RST: {
			Machine_callTo(m, instr.imm8 * 8);
			m->timer += 12;
		} break;


		case OPCODES_CMP: {
			uint8_t bak = m->a;
			Machine_subToAccumulator(m, *Machine_getReg8(m, instr.src8), false);
			m->a = bak;
			m->timer += (instr.src8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_CPI: {
			uint8_t bak = m->a;
			Machine_subToAccumulator(m, instr.imm8, false);
			m->a = bak;
			m->timer += 7;
		} break;
		case OPCODES_ANA: {
			Machine_andAccumulator(m, *Machine_getReg8(m, instr.src8));
			m->timer += (instr.src8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_ANI: {
			Machine_andAccumulator(m, instr.imm8);
			m->timer += 7;
		} break;
		case OPCODES_ORA: {
			Machine_orAccumulator(m, *Machine_getReg8(m, instr.src8));
			m->timer += (instr.src8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_ORI:  {
			Machine_orAccumulator(m, instr.imm8);
			m->timer += 7;
		} break;
		case OPCODES_XRA: {
			Machine_xorAccumulator(m, *Machine_getReg8(m, instr.src8));
			m->timer += (instr.src8 == REG8_M) ? 7 : 4;
		} break;
		case OPCODES_XRI: {
			Machine_xorAccumulator(m, instr.imm8);
			m->timer += 7;
		} break;
		case OPCODES_RLC: {
			Machine_setFlag(m, FLAGBITS_CARRY, (m->a & 0x80) != 0);
			m->a = (m->a << 1) | (m->a >> 7);
			m->timer += 4;
		} break;
		case OPCODES_RRC: {
			Machine_setFlag(m, FLAGBITS_CARRY, (m->a & 0x01) != 0);
			m->a = (m->a >> 1) | (m->a << 7);
			m->timer += 4;
		} break;
		case OPCODES_RAL: {
			bool low = Machine_getFlag(m, FLAGBITS_CARRY);
			Machine_setFlag(m, FLAGBITS_CARRY, (m->a & 0x80) != 0);
			m->a = (m->a << 1) | (low ? 0x01 : 0);
			m->timer += 4;
		} break;
		case OPCODES_RAR: {
			bool high = Machine_getFlag(m, FLAGBITS_CARRY);
			Machine_setFlag(m, FLAGBITS_CARRY, (m->a & 0x1) != 0);
			m->a = (high ? 0x80 : 0) | (m->a >> 1);
			m->timer += 4;
		} break;
		case OPCODES_CMA: {
			m->a = m->a ^ 0xFF;
			m->timer += 4;
		} break;
		case OPCODES_CMC: {
			Machine_setFlag(m, FLAGBITS_CARRY, !Machine_getFlag(m, FLAGBITS_CARRY));
			m->timer += 4;
		} break;
		case OPCODES_STC: {
			Machine_setFlag(m, FLAGBITS_CARRY, true);
			m->timer += 4;
		} break;


		case OPCODES_NOP: {
			m->timer += 4;
		} break;
		case OPCODES_HLT: {
			m->pc--; // So the instruction will run again
			m->timer += 4;
		} break;
		case OPCODES_DI: {
			m->intrEnable = false;
			m->timer += 4;
		} break;
		case OPCODES_EI: {
			m->intrEnable = true;
			m->timer += 4;
		} break;
		case OPCODES_RIM: {
			m->a = 0;
			if (m->intrMask55) m->a |= (1 << 0);
			if (m->intrMask65) m->a |= (1 << 1);
			if (m->intrMask75) m->a |= (1 << 2);
			if (m->intrEnable) m->a |= (1 << 3);
			if (m->intrPend55) m->a |= (1 << 4);
			if (m->intrPend65) m->a |= (1 << 5);
			if (m->intrPend75) m->a |= (1 << 6);
			if (m->serialIn) m->a |= (1 << 7);
			m->timer += 4;
		} break;
		case OPCODES_SIM: {
			m->intrMask55 = (m->a & (1 << 0)) != 0;
			m->intrMask65 = (m->a & (1 << 1)) != 0;
			m->intrMask75 = (m->a & (1 << 2)) != 0;
			m->intrEnable = (m->a & (1 << 3)) != 0;
			if ((m->a & (1 << 4)) != 0)
				m->intrPend75 = false; // TODO?
			m->serialOutEnabled = (m->a & (1 << 6)) != 0;
			m->serialOut = (m->a & (1 << 7)) != 0;
			m->timer += 4;
		} break;

		case OPCODES_UNKNOWN: default: {
			fprintf(stderr, "Unknown opcode (internal ID is %.2X)\n", instr.opcode);
			fprintf(stderr, "Imm8 is %.2X.", instr.imm8);
		} break;
	}
}

/* Prints some information to debug problems. */
static void Machine_debugPrint(const Machine *m) {
	fprintf(stderr,
		"A=%.2X\n"
		"F=%.2X\n"
		"B=%.2X\n"
		"C=%.2X\n"
		"D=%.2X\n"
		"E=%.2X\n"
		"HL=%.4X\n"
		"SP=%.4X\n"
		"M[HL]=%.2X\n"
		"PC=%.4X\n",
		m->a, m->f, m->b, m->c, m->d, m->e, m->hl, m->sp, Machine_readMemory8(m, m->hl), m->pc);
}

/* Runs for at least 'nCycles' cycles. */
void Machine_runCycles(Machine *m, uint64_t nCycles) {
	for (uint64_t dstTimer = m->timer + nCycles; m->timer < dstTimer; ) {
		// Handle events
		while (!EventQueue_isEmpty(&m->eventQueue) && EventQueue_topPriority(&m->eventQueue) <= m->timer) {
			uint64_t time = EventQueue_topPriority(&m->eventQueue);
			Device *device = EventQueue_pop(&m->eventQueue);

			device->onEvent(device, time);
		}

		// Jump to interrupt vectors (Note that only one of those ifs can happen,
		// since jumpToInterruptVector will set intrEnable to zero).
		if (m->intrPendTrap) {
			m->intrPendTrap = false;
			Machine_jumpToInterruptVector(m, 0x0024);
		}
		if (m->intrPend75 && !m->intrMask75 && m->intrEnable) {
			m->intrPend75 = false;
			Machine_jumpToInterruptVector(m, 0x003C);
		}
		if (m->intrPend65 && !m->intrMask65 && m->intrEnable) {
			m->intrPend65 = false;
			Machine_jumpToInterruptVector(m, 0x0034);
		}
		if (m->intrPend55 && !m->intrMask55 && m->intrEnable) {
			m->intrPend55 = false;
			Machine_jumpToInterruptVector(m, 0x002C);
		}

		// Debug mode
		if (m->debugMode)
			Machine_debugPrint(m);

		// Run another instruction
		Instruction instr = Machine_decode(m);
		if (instr.opcode == OPCODES_HLT) { // Idle skip
			/* In order to avoid hogging the CPU while waiting for events or finished,
			 * handle HLT specially and just skip the timer until the next event. */
			uint64_t target = !EventQueue_isEmpty(&m->eventQueue) ? EventQueue_topPriority(&m->eventQueue) : dstTimer;
			uint64_t distance = target - m->timer;
			while ((distance % 4) != 0) distance++;
			m->timer += distance;
			m->pc -= 1;
		} else {
			Machine_runInstruction(m, instr);
		}
	}
}

static void Device_Timer_onAttach(Device *d, Machine *machine) {
	Device_Timer *dt = (Device_Timer *)d;
	dt->device.machine = machine;

	// Add the first event
	Machine_addEvent(dt->device.machine, dt->period, d);
}

static void Device_Timer_onEvent(Device *d, uint64_t eventTime) {
	Device_Timer *dt = (Device_Timer *)d;

	// Add the next event
	Machine_addEvent(dt->device.machine, eventTime + dt->period, d);

	// Trigger an interrupt of the specified type
	Machine_requestInterrupt(dt->device.machine, dt->intrType);
}

static void Device_Timer_onOut(Device *d, uint8_t port) {
}

void Device_Timer_init(Device_Timer *dt, uint64_t period, INTERRUPTTYPE intrType) {
	dt->device.machine = NULL;
	dt->device.onAttach = &Device_Timer_onAttach;
	dt->device.onEvent = &Device_Timer_onEvent;
	dt->device.onOut = &Device_Timer_onOut;
	dt->period = period;
	dt->intrType = intrType;
}
