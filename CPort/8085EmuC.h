#ifndef _8085EMUC_H
#define _8085EMUC_H

/**************************************
 * JAVASCRIPT 8085 SIMULATOR - C PORT *
 **************************************
 * By joanbrugueram.
 *
 * The architecture is inspired by a 8085 simulator by Angel Manuel Gómez García et al.
 *
 * First public release on 20th April 2013, under the WTFPL license.
 * C port released on 6th June 2013, also under the WTFPL license.
 */

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

struct Machine;

/* Base structure for devices attaching to a machine. */
typedef struct Device {
	struct Machine *machine;

	void (*onAttach)(struct Device *d, struct Machine *machine);
	void (*onEvent)(struct Device *d, uint64_t time);
	void (*onOut)(struct Device *d, uint8_t port);
} Device;

/* Implementation of an event queue (priority queue) using a min heap with binary trees. */
struct EventQueue_Event {
	uint64_t time;
	Device *device;
};

typedef struct EventQueue {
	struct EventQueue_Event events[64];
	size_t nEvents;
} EventQueue;

/* Kinds of 8085 interrupt. */
typedef enum INTERRUPTTYPE {
	INTERRUPTTYPE_TRAP = 0,
	INTERRUPTTYPE_RST75 = 1,
	INTERRUPTTYPE_RST65 = 2,
	INTERRUPTTYPE_RST55 = 3
} INTERRUPTTYPE;

/* A machine with (usually) 65536 bytes of RAM, 256 input ports, 256 output ports, and a 8085 processor.
 * Additional devices, such as screens, keyboards and interrupt timers, can be attached. */
typedef struct Machine {
	// TODO this is horribly compiler dependent and endian dependent

	/* PSW register (Accumulator + Flags)*/
	union {
		struct {
			#ifdef BIG_ENDIAN
			uint8_t a;
			uint8_t f;
			#else
			uint8_t f;
			uint8_t a;
			#endif
		};
		uint16_t psw;
	};

	/* B-C register pair */
	union {
		struct {
			#ifdef BIG_ENDIAN
			uint8_t b;
			uint8_t c;
			#else
			uint8_t c;
			uint8_t b;
			#endif
		};
		uint16_t bc;
	};

	/* D-E register pair */
	union {
		struct {
			#ifdef BIG_ENDIAN
			uint8_t d;
			uint8_t e;
			#else
			uint8_t e;
			uint8_t d;
			#endif
		};
		uint16_t de;
	};

	/* H-L (High-Low) register pair */
	union {
		struct {
			#ifdef BIG_ENDIAN
			uint8_t h;
			uint8_t l;
			#else
			uint8_t l;
			uint8_t h;
			#endif
		};
		uint16_t hl;
	};

	/* Stack pointer */
	uint16_t sp;

	/* Program counter */
	uint16_t pc;

	/* INTERRUPT BITS */
	/* Set to zero to disable all RST interrupts. */
	bool intrEnable;
	/* Mask for RST 5.5 interrupt (set to 1 to disable handling). */
	bool intrMask55;
	/* Mask for RST 6.5 interrupt (set to 1 to disable handling). */
	bool intrMask65;
	/* Mask for RST 7.5 interrupt (set to 1 to disable handling). */
	bool intrMask75;
	/* Specifies if a RST 5.5 interrupt is pending. */
	bool intrPend55;
	/* Specifies if a RST 6.5 interrupt is pending. */
	bool intrPend65;
	/* Specifies if a RST 7.5 interrupt is pending. */
	bool intrPend75;
	/* Specifies if a TRAP interrupt is pending. */
	bool intrPendTrap;

	/* SERIAL BITS */
	/* Bit in the serial input port */
	bool serialIn;
	/* Set if a bit has been sent to the output port */
	bool serialOutEnabled;
	/* Bit in the serial output port */
	bool serialOut;

	/* Memory */
	uint8_t memory[0x10000];
	/* Input ports */
	uint8_t inp[0x100];
	/* Output ports */
	uint8_t outp[0x100];

	/* Event queue */
	EventQueue eventQueue;
	/* Number of cycles elapsed. */
	uint64_t timer;

	/* If true, information about the machine is shown after every instruction. */
	bool debugMode;

	/* Number of attached devices */
	size_t nDevices;
	/* Array of attached devices */
	Device *devices[8];
} Machine;

/* Reset the machine to its initial state. */
void Machine_reset(Machine *m);

/* Attach a device to this machine. */
void Machine_attachDevice(Machine *m, Device *d);

/* Load a program from an object file */
/* FIXME: Take a "const char *" and don't trash the string. */
bool Machine_loadProgram(Machine *m, char *program);

/* Read a 8-bit value from the memory. */
uint8_t Machine_readMemory8(const Machine *m, uint16_t addr);

/* Write a 8-bit value to the memory. */
void Machine_writeMemory8(Machine *m, uint16_t addr, uint8_t value);

/* Read a 16-bit value from the specified memory address.
 * The first byte is the low byte and the second byte is the high byte. */
uint16_t Machine_readMemory16(const Machine *m, uint16_t addr);

/* Write a 16-bit value to the specified memory address.
 * The first byte is the low byte and the second byte is the high byte. */
void Machine_writeMemory16(Machine *m, uint16_t addr, uint16_t value);

/* Set the specified input port to the specified value. */
void Machine_setInPort(Machine *m, uint8_t port, uint8_t value);

/* Get the value of the specified output port. */
uint8_t Machine_getOutPort(Machine *m, uint8_t port);

/* Requests an interrupt to happen (sets the pending bits). 
 * (It may or may not happen depending on the masks.) */
void Machine_requestInterrupt(Machine *m, INTERRUPTTYPE type);

/* Adds an event from the specified device, which will happen at the specified cycle. */
void Machine_addEvent(Machine *m, uint64_t time, Device *device);

/* Runs for at least 'nCycles' cycles. */
void Machine_runCycles(Machine *m, uint64_t nCycles);

/* A timer attached to a 8085 machine. Triggers an interrupt periodically. */
typedef struct Device_Timer {
	Device device;
	uint64_t period;
	INTERRUPTTYPE intrType;
} Device_Timer;

void Device_Timer_init(Device_Timer *dt, uint64_t period, INTERRUPTTYPE intrType);

#endif // _8085EMUC_H