#include "8085EmuC.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define I8085_CLOCK_FREQUENCY   (3140000UL)
#define N_SLICES_PER_SEC        5

typedef struct Device_Console {
	Device device;
	uint8_t port;
} Device_Console;

static void Device_Console_onAttach(Device *d, Machine *machine) {
	Device_Console *dc = (Device_Console *)d;
	dc->device.machine = machine;
}

static void Device_Console_onEvent(Device *d, uint64_t eventTime) { }
static void Device_Console_onOut(Device *d, uint8_t port) {
	Device_Console *dc = (Device_Console *)d;
	if (port == dc->port) {
		putchar(dc->device.machine->outp[port]);
		fflush(stdout);
	}
}

static void Device_Console_init(Device_Console *dc, uint8_t port) {
	dc->device.machine = NULL;
	dc->device.onAttach = &Device_Console_onAttach;
	dc->device.onEvent = &Device_Console_onEvent;
	dc->device.onOut = &Device_Console_onOut;
	dc->port = port;
}

static Machine m;
static Device_Timer dt;
static Device_Console dc;

int main() {
	// Set up machine
	Machine_reset(&m);
	Device_Timer_init(&dt, I8085_CLOCK_FREQUENCY, INTERRUPTTYPE_TRAP); // ~1 second CPU time
	Machine_attachDevice(&m, (Device *)&dt);
	Device_Console_init(&dc, 0xFF);
	Machine_attachDevice(&m, (Device *)&dc);

	char program[] =
		".ORG 00h 01h\n"
		"3Eh 30h \n"
		"C3h 02h 01h \n"
		".ORG 24h 00h\n"
		"D3h FFh \n"
		"3Ch \n"
		"FEh 3Ah \n"
		"F8h \n"
		"3Eh 30h \n"
		"C9h \n"
	;
	Machine_loadProgram(&m, program);

	while (true) {
		clock_t coreTimerStart = clock();

		// Run the machine for some cycles
		//printf("SliceStart\n");
		Machine_runCycles(&m, I8085_CLOCK_FREQUENCY / N_SLICES_PER_SEC);
		//printf("SliceEnd\n");

		// Wait until the end of the slice if needed
		while ((clock() - coreTimerStart) < CLOCKS_PER_SEC/N_SLICES_PER_SEC);
	}
}
