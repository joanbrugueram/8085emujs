/* Designed for Microstick II by Microchip by joanbrugueram.
 * You need to set up a terminal session through USB.
 * Connect the pins as illustrated below:
 * >>>>>>>>
 
    *     *
    *     *
    *     *
    *     *
    *     *
    *     *
    *     * -> RX
    *     * -> TX
    *     *
    *     *
    *     *
    *     *
    *     *
    *     *
 
    [vvv]
    [USB]
    [vvv]
 
 * <<<<<<<<
 * Set up terminal with Baud rate = 38400, Data bits = 8, Stop bits = 1, Parity = None.
 */
#include "8085EmuC.h"
#include <stdbool.h>
#include <stdlib.h>

#include <plib.h>
#pragma config   FNOSC       = FRCPLL
#pragma config   FPLLIDIV    = DIV_2
#pragma config   FPLLMUL     = MUL_20
#pragma config   FPLLODIV    = DIV_2
#pragma config   FPBDIV      = DIV_1

#define GetSystemClock()        (40000000UL)
#define GetPeripheralClock()    (GetSystemClock())
#define CORE_TICK_RATE          (GetSystemClock() / 2) // 1 sec

#define I8085_CLOCK_FREQUENCY   (3140000UL)
#define N_SLICES_PER_SEC        5

/******************************************
 * CONSOLE OUTPUT DEVICE (UART2 TRANSMIT) *
 ******************************************/
typedef struct Device_ConsoleOutput {
	Device device;
	uint8_t port;
} Device_ConsoleOutput;

static void Device_ConsoleOutput_onAttach(Device *d, Machine *machine) {
	Device_ConsoleOutput *dco = (Device_ConsoleOutput *)d;
	dco->device.machine = machine;
}

static void Device_ConsoleOutput_onEvent(Device *d, uint64_t eventTime) { }
static void Device_ConsoleOutput_onOut(Device *d, uint8_t port) {
	Device_ConsoleOutput *dco = (Device_ConsoleOutput *)d;
	if (port == dco->port) {
		BYTE data = Machine_getOutPort(dco->device.machine, dco->port);
		// Print console (UART2 transmit)
		while(!UARTTransmitterIsReady(UART2));
		UARTSendDataByte(UART2, data); 
	}
}

static void Device_ConsoleOutput_init(Device_ConsoleOutput *dco, uint8_t port) {
	dco->device.machine = NULL;
	dco->device.onAttach = &Device_ConsoleOutput_onAttach;
	dco->device.onEvent = &Device_ConsoleOutput_onEvent;
	dco->device.onOut = &Device_ConsoleOutput_onOut;
	dco->port = port;
}

/****************************************
 * CONSOLE INPUT DEVICE (UART2 RECIEVE) *
 ****************************************/
#define KEYBUFFER_LENGTH 0x800

typedef struct Device_ConsoleInput {
	Device device;
	uint8_t port;
	uint8_t keyBuffer[KEYBUFFER_LENGTH]; // Circular queue
	size_t keyBufferRead; // Read index into keyBuffer
	size_t keyBufferWrite; // Write index into keyBuffer
} Device_ConsoleInput;

static void Device_ConsoleInput_onAttach(Device *d, Machine *machine) {
	Device_ConsoleInput *dci = (Device_ConsoleInput *)d;
	dci->device.machine = machine;

	// Add initial polling event
	Machine_addEvent(dci->device.machine, 10000, d);
}

static void Device_ConsoleInput_onEvent(Device *d, uint64_t eventTime) {
	Device_ConsoleInput *dci = (Device_ConsoleInput *)d;

	// Poll key buffer
	if (dci->keyBufferRead%KEYBUFFER_LENGTH != dci->keyBufferWrite%KEYBUFFER_LENGTH) { // Not empty
		uint8_t key = dci->keyBuffer[dci->keyBufferRead++%KEYBUFFER_LENGTH];
		Machine_setInPort(dci->device.machine, dci->port, key);
		Machine_requestInterrupt(dci->device.machine, INTERRUPTTYPE_TRAP);
	}

	// Add next polling event
	Machine_addEvent(dci->device.machine, eventTime + 10000, d);
}
static void Device_ConsoleInput_onOut(Device *d, uint8_t port) { }

static void Device_ConsoleInput_init(Device_ConsoleInput *dco, uint8_t port) {
	dco->device.machine = NULL;
	dco->device.onAttach = &Device_ConsoleInput_onAttach;
	dco->device.onEvent = &Device_ConsoleInput_onEvent;
	dco->device.onOut = &Device_ConsoleInput_onOut;
	dco->port = port;
	dco->keyBufferRead = 0;
	dco->keyBufferWrite = 0;
}

/*****************
 * MACHINE SETUP *
 *****************/
static Machine m;
static Device_ConsoleOutput dco;
static Device_ConsoleInput dci;

void __ISR(_UART_2_VECTOR, ipl2) IntUart2Handler(void) {
	// Is this an RX interrupt?
	if (INTGetFlag(INT_SOURCE_UART_RX(UART2))) {
		// Add console input to keyboard buffer
		if (UARTReceivedDataIsAvailable(UART2)) {
			BYTE data = UARTGetDataByte(UART2);
			if ((dci.keyBufferWrite+1)%KEYBUFFER_LENGTH != dci.keyBufferRead%KEYBUFFER_LENGTH) { // No overflow
				dci.keyBuffer[dci.keyBufferWrite++%KEYBUFFER_LENGTH] = data;
			}
		}

		// Clear the RX interrupt Flag
		INTClearFlag(INT_SOURCE_UART_RX(UART2));
	}

	// We don't care about TX interrupt
	if (INTGetFlag(INT_SOURCE_UART_TX(UART2))) {
		INTClearFlag(INT_SOURCE_UART_TX(UART2));
	}
}


int main() {
	SYSTEMConfig(GetSystemClock(), SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);

	// Set up LED
	mPORTAClearBits(BIT_0);
	mPORTASetPinsDigitalOut(BIT_0);

	// Set up UART PINS (USB)
	mPORTBClearBits(BIT_10);   
	mPORTBSetPinsDigitalOut(BIT_10); // Set PB10(Tx) as output
	mPORTBSetPinsDigitalIn (BIT_11); // Set PB11(Rx) as input
 
	PPSUnLock;                       // Allow PIN Mapping
		PPSOutput(4, RPB10, U2TX);   // MAP Tx to PB10
		PPSInput (2, U2RX, RPB11);   // MAP Rx to PB11
	PPSLock;                         // Prevent Accidental Mapping
 
	// Configure UART2
	UARTConfigure(UART2, UART_ENABLE_PINS_TX_RX_ONLY);
	UARTSetLineControl(UART2, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
	UARTSetDataRate(UART2, GetPeripheralClock(), 38400);
	UARTSetFifoMode(UART2, UART_INTERRUPT_ON_RX_NOT_EMPTY);
	UARTEnable(UART2, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));   
	U2STAbits.UTXINV = 1;
	U2MODEbits.RXINV = 1;

	// Enable interrupts
	INTEnable(INT_U2RX, INT_ENABLED);
	INTEnableSystemMultiVectoredInt();
	INTSetVectorPriority(INT_UART_2_VECTOR, INT_PRIORITY_LEVEL_2);
	INTSetVectorSubPriority(INT_UART_2_VECTOR, INT_SUB_PRIORITY_LEVEL_0);

	// Set up machine
	Machine_reset(&m);
	Device_ConsoleOutput_init(&dco, 0xFF);
	Machine_attachDevice(&m, (Device *)&dco);
	Device_ConsoleInput_init(&dci, 0xFF);
	Machine_attachDevice(&m, (Device *)&dci);

	char program[] =
/*
.define
	PNT 2Eh
	RTL 2Dh

.data 1000h
	; Taula lookup ASCII-Morse (lookupAsciiMorse + 8 * caracterASCII)
	lookupAsciiMorse:
	; 0x00-0x10
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db 0Ah,  0,  0,  0,  0,  0,  0,  0 ; LF
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db 0Dh,  0,  0,  0,  0,  0,  0,  0 ; CR
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	; 0x10-0x20
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	; 0x20-0x30
	db ' ',' ',  0,  0,  0,  0,  0,  0 ; Espai
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db PNT,RTL,RTL,RTL,RTL,PNT,' ',  0 ; '
	db RTL,PNT,RTL,RTL,PNT,RTL,' ',  0 ; (
	db RTL,PNT,RTL,RTL,PNT,RTL,' ',  0 ; )
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db RTL,RTL,PNT,PNT,RTL,RTL,' ',  0 ; ,
	db RTL,PNT,PNT,PNT,PNT,RTL,' ',  0 ; -
	db PNT,RTL,PNT,RTL,PNT,RTL,' ',  0 ; .
	db RTL,PNT,PNT,RTL,PNT,' ',  0,  0 ; /
	; 0x30-0x40
	db RTL,RTL,RTL,RTL,RTL,' ',  0,  0 ; 0
	db PNT,RTL,RTL,RTL,RTL,' ',  0,  0 ; 1
	db PNT,PNT,RTL,RTL,RTL,' ',  0,  0 ; 2
	db PNT,PNT,PNT,RTL,RTL,' ',  0,  0 ; 3
	db PNT,PNT,PNT,PNT,RTL,' ',  0,  0 ; 4
	db PNT,PNT,PNT,PNT,PNT,' ',  0,  0 ; 5
	db RTL,PNT,PNT,PNT,PNT,' ',  0,  0 ; 6
	db RTL,RTL,PNT,PNT,PNT,' ',  0,  0 ; 7
	db RTL,RTL,RTL,PNT,PNT,' ',  0,  0 ; 8
	db RTL,RTL,RTL,RTL,PNT,' ',  0,  0 ; 9
	db RTL,RTL,RTL,PNT,PNT,PNT,' ',  0 ; :
	db RTL,PNT,RTL,PNT,RTL,' ',  0,  0 ; ;
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db PNT,PNT,RTL,RTL,PNT,PNT,' ',  0 ; ?
	; 0x40-0x50
	db   0,  0,  0,  0,  0,  0,  0,  0
	db PNT,RTL,' ',  0,  0,  0,  0,  0 ; A
	db RTL,PNT,PNT,PNT,' ',  0,  0,  0 ; B
	db RTL,PNT,RTL,PNT,' ',  0,  0,  0 ; C
	db RTL,PNT,PNT,' ',  0,  0,  0,  0 ; D
	db PNT,' ',  0,  0,  0,  0,  0,  0 ; E
	db PNT,PNT,RTL,PNT,' ',  0,  0,  0 ; F
	db RTL,RTL,PNT,' ',  0,  0,  0,  0 ; G
	db PNT,PNT,PNT,PNT,' ',  0,  0,  0 ; H
	db PNT,PNT,' ',  0,  0,  0,  0,  0 ; I
	db PNT,RTL,RTL,RTL,' ',  0,  0,  0 ; J
	db RTL,PNT,RTL,' ',  0,  0,  0,  0 ; K
	db PNT,RTL,PNT,PNT,' ',  0,  0,  0 ; L
	db RTL,RTL,' ',  0,  0,  0,  0,  0 ; M
	db RTL,PNT,' ',  0,  0,  0,  0,  0 ; N
	db RTL,RTL,RTL,' ',  0,  0,  0,  0 ; O
	; 0x50-0x60
	db PNT,RTL,RTL,PNT,' ',  0,  0,  0 ; P
	db RTL,RTL,PNT,RTL,' ',  0,  0,  0 ; Q
	db PNT,RTL,PNT,' ',  0,  0,  0,  0 ; R
	db PNT,PNT,PNT,' ',  0,  0,  0,  0 ; S
	db RTL,' ',  0,  0,  0,  0,  0,  0 ; T
	db PNT,PNT,RTL,' ',  0,  0,  0,  0 ; U
	db PNT,PNT,PNT,RTL,' ',  0,  0,  0 ; V
	db PNT,RTL,RTL,' ',  0,  0,  0,  0 ; W
	db RTL,PNT,PNT,RTL,' ',  0,  0,  0 ; X
	db RTL,PNT,RTL,RTL,' ',  0,  0,  0 ; Y
	db RTL,RTL,PNT,PNT,' ',  0,  0,  0 ; Z
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db PNT,PNT,RTL,RTL,PNT,RTL,' ',  0 ; _
	; 0x60-0x70
	db   0,  0,  0,  0,  0,  0,  0,  0
	db PNT,RTL,' ',  0,  0,  0,  0,  0 ; a
	db RTL,PNT,PNT,PNT,' ',  0,  0,  0 ; b
	db RTL,PNT,RTL,PNT,' ',  0,  0,  0 ; c
	db RTL,PNT,PNT,' ',  0,  0,  0,  0 ; d
	db PNT,' ',  0,  0,  0,  0,  0,  0 ; e
	db PNT,PNT,RTL,PNT,' ',  0,  0,  0 ; f
	db RTL,RTL,PNT,' ',  0,  0,  0,  0 ; g
	db PNT,PNT,PNT,PNT,' ',  0,  0,  0 ; h
	db PNT,PNT,' ',  0,  0,  0,  0,  0 ; i
	db PNT,RTL,RTL,RTL,' ',  0,  0,  0 ; j
	db RTL,PNT,RTL,' ',  0,  0,  0,  0 ; k
	db PNT,RTL,PNT,PNT,' ',  0,  0,  0 ; l
	db RTL,RTL,' ',  0,  0,  0,  0,  0 ; m
	db RTL,PNT,' ',  0,  0,  0,  0,  0 ; n
	db RTL,RTL,RTL,' ',  0,  0,  0,  0 ; o
	; 0x70-0x80
	db PNT,RTL,RTL,PNT,' ',  0,  0,  0 ; p
	db RTL,RTL,PNT,RTL,' ',  0,  0,  0 ; q
	db PNT,RTL,PNT,' ',  0,  0,  0,  0 ; r
	db PNT,PNT,PNT,' ',  0,  0,  0,  0 ; s
	db RTL,' ',  0,  0,  0,  0,  0,  0 ; t
	db PNT,PNT,RTL,' ',  0,  0,  0,  0 ; u
	db PNT,PNT,PNT,RTL,' ',  0,  0,  0 ; v
	db PNT,RTL,RTL,' ',  0,  0,  0,  0 ; w
	db RTL,PNT,PNT,RTL,' ',  0,  0,  0 ; x
	db RTL,PNT,RTL,RTL,' ',  0,  0,  0 ; y
	db RTL,RTL,PNT,PNT,' ',  0,  0,  0 ; z
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0
	db   0,  0,  0,  0,  0,  0,  0,  0

	missatge:
	db 'C','o','d','i','f','i','c','a','d','o','r',' ','M','o','r','s','e',' ','p','e','r',' ','8','0','8','5',0Dh,0Ah
	db 'I','n','t','r','o','d','u','e','i','x',' ','u','n',' ','t','e','x','t',3Ah,0Dh,0Ah
	db 0

.org 100h
	LXI SP, 0h

	; Imprimir missatge d'informació
	lxi H, missatge
	call imprimirMissatgeConsola

	hlt

; Imprimeix un missatge acabat en 0 a la consola. Punter al missatge a HL.
; Canvia HL (fins al final del missatge) i A (a zero).
imprimirMissatgeConsola:
	loopmissatge:
		; Movem caràcter a imprimir
		mov A, M
		; Mirem si és el fi del missatge
		ana A
		rz

		; Treiem el caràcter a la consola
		out FFh
		; Passem al següent caràcter
		inx H
		jmp loopmissatge

.org 24h
	; Llegim el caràcter de la consola
	in FFh

	; Sortim directament si és >= 0x80
	cpi 80h
	rp

	; Busquem la codificació Morse a la taula
	mvi D, 0
	mov E, A
	lxi H, lookupAsciiMorse
	dad D
	dad D
	dad D
	dad D
	dad D
	dad D
	dad D
	dad D

	; Imprimim el codi
	jmp imprimirMissatgeConsola
*/
".DATA 00h 10h\n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"0Ah 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"0Dh 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"20h 20h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Dh 2Dh 2Dh 2Dh 2Eh 20h 00h \n"
"2Dh 2Eh 2Dh 2Dh 2Eh 2Dh 20h 00h \n"
"2Dh 2Eh 2Dh 2Dh 2Eh 2Dh 20h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"2Dh 2Dh 2Eh 2Eh 2Dh 2Dh 20h 00h \n"
"2Dh 2Eh 2Eh 2Eh 2Eh 2Dh 20h 00h \n"
"2Eh 2Dh 2Eh 2Dh 2Eh 2Dh 20h 00h \n"
"2Dh 2Eh 2Eh 2Dh 2Eh 20h 00h 00h \n"
"2Dh 2Dh 2Dh 2Dh 2Dh 20h 00h 00h \n"
"2Eh 2Dh 2Dh 2Dh 2Dh 20h 00h 00h \n"
"2Eh 2Eh 2Dh 2Dh 2Dh 20h 00h 00h \n"
"2Eh 2Eh 2Eh 2Dh 2Dh 20h 00h 00h \n"
"2Eh 2Eh 2Eh 2Eh 2Dh 20h 00h 00h \n"
"2Eh 2Eh 2Eh 2Eh 2Eh 20h 00h 00h \n"
"2Dh 2Eh 2Eh 2Eh 2Eh 20h 00h 00h \n"
"2Dh 2Dh 2Eh 2Eh 2Eh 20h 00h 00h \n"
"2Dh 2Dh 2Dh 2Eh 2Eh 20h 00h 00h \n"
"2Dh 2Dh 2Dh 2Dh 2Eh 20h 00h 00h \n"
"2Dh 2Dh 2Dh 2Eh 2Eh 2Eh 20h 00h \n"
"2Dh 2Eh 2Dh 2Eh 2Dh 20h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Dh 2Dh 2Eh 2Eh 20h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Dh 20h 00h 00h 00h 00h 00h \n"
"2Dh 2Eh 2Eh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Eh 2Dh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Eh 2Eh 20h 00h 00h 00h 00h \n"
"2Eh 20h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Dh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Dh 2Eh 20h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Eh 2Eh 20h 00h 00h 00h \n"
"2Eh 2Eh 20h 00h 00h 00h 00h 00h \n"
"2Eh 2Dh 2Dh 2Dh 20h 00h 00h 00h \n"
"2Dh 2Eh 2Dh 20h 00h 00h 00h 00h \n"
"2Eh 2Dh 2Eh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Dh 20h 00h 00h 00h 00h 00h \n"
"2Dh 2Eh 20h 00h 00h 00h 00h 00h \n"
"2Dh 2Dh 2Dh 20h 00h 00h 00h 00h \n"
"2Eh 2Dh 2Dh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Dh 2Eh 2Dh 20h 00h 00h 00h \n"
"2Eh 2Dh 2Eh 20h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Eh 20h 00h 00h 00h 00h \n"
"2Dh 20h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Dh 20h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Eh 2Dh 20h 00h 00h 00h \n"
"2Eh 2Dh 2Dh 20h 00h 00h 00h 00h \n"
"2Dh 2Eh 2Eh 2Dh 20h 00h 00h 00h \n"
"2Dh 2Eh 2Dh 2Dh 20h 00h 00h 00h \n"
"2Dh 2Dh 2Eh 2Eh 20h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Dh 2Dh 2Eh 2Dh 20h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Dh 20h 00h 00h 00h 00h 00h \n"
"2Dh 2Eh 2Eh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Eh 2Dh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Eh 2Eh 20h 00h 00h 00h 00h \n"
"2Eh 20h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Dh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Dh 2Eh 20h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Eh 2Eh 20h 00h 00h 00h \n"
"2Eh 2Eh 20h 00h 00h 00h 00h 00h \n"
"2Eh 2Dh 2Dh 2Dh 20h 00h 00h 00h \n"
"2Dh 2Eh 2Dh 20h 00h 00h 00h 00h \n"
"2Eh 2Dh 2Eh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Dh 20h 00h 00h 00h 00h 00h \n"
"2Dh 2Eh 20h 00h 00h 00h 00h 00h \n"
"2Dh 2Dh 2Dh 20h 00h 00h 00h 00h \n"
"2Eh 2Dh 2Dh 2Eh 20h 00h 00h 00h \n"
"2Dh 2Dh 2Eh 2Dh 20h 00h 00h 00h \n"
"2Eh 2Dh 2Eh 20h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Eh 20h 00h 00h 00h 00h \n"
"2Dh 20h 00h 00h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Dh 20h 00h 00h 00h 00h \n"
"2Eh 2Eh 2Eh 2Dh 20h 00h 00h 00h \n"
"2Eh 2Dh 2Dh 20h 00h 00h 00h 00h \n"
"2Dh 2Eh 2Eh 2Dh 20h 00h 00h 00h \n"
"2Dh 2Eh 2Dh 2Dh 20h 00h 00h 00h \n"
"2Dh 2Dh 2Eh 2Eh 20h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"00h 00h 00h 00h 00h 00h 00h 00h \n"
"43h 4Fh 44h 49h 46h 49h 43h 41h 44h 4Fh 52h 20h 4Dh 4Fh 52h 53h 45h 20h 50h 45h 52h 20h 38h 30h 38h 35h 0Dh 0Ah \n"
"49h 4Eh 54h 52h 4Fh 44h 55h 45h 49h 58h 20h 55h 4Eh 20h 54h 45h 58h 54h 3Ah 0Dh 0Ah \n"
"00h \n"
".ORG 00h 01h\n"
"31h 00h 00h \n"
"21h 00h 14h \n"
"CDh 0Ah 01h \n"
"76h \n"
"7Eh \n"
"A7h \n"
"C8h \n"
"D3h FFh \n"
"23h \n"
"C3h 0Ah 01h \n"
".ORG 24h 00h\n"
"DBh FFh \n"
"FEh 80h \n"
"F0h \n"
"16h 00h \n"
"5Fh \n"
"21h 00h 10h \n"
"19h \n"
"19h \n"
"19h \n"
"19h \n"
"19h \n"
"19h \n"
"19h \n"
"19h \n"
"C3h 0Ah 01h \n"

	;
	Machine_loadProgram(&m, program);

	while (true) {
		unsigned int coreTimerStart = ReadCoreTimer();

		// Run the machine for some cycles
		mPORTASetBits(BIT_0);
		Machine_runCycles(&m, I8085_CLOCK_FREQUENCY / N_SLICES_PER_SEC);
		mPORTAClearBits(BIT_0);

		// Wait until the end of the slice if needed
		while ((ReadCoreTimer() - coreTimerStart) < CORE_TICK_RATE/N_SLICES_PER_SEC);
	}
}
