///* 8085 Assembler Parser by joanbrugueram. Licensed under the WTFPL.
// * ---------------------
// * Parses a 8085 program. At the end, returns an array of commands.
// * Each command is an structure which has the 'command' property set, plus maybe some parameters.
// * Here's a list of all commands returned plus parameters:
// * - .DATA(Expression address)
// * - .ORG(Expression address)
// * - DEFINELABEL(IdentifierExpression name)
// * - DEFINECONSTANT(IdentifierExpression name, Expression value)
// * - DB(Expression[] data)
// * - DW(Expression[] data)
// * - DS(String data)
// * - PUTINSTRNIL(String op)
// * - PUTINSTRIMM3IMPLICIT(String op, Expression imm3)
// * - PUTINSTRREG8(String op, Reg8 reg8)
// * - PUTINSTRDST8SRC8(String op, Reg8 dst8, Reg8 src8)
// * - PUTINSTRIMM8(String op, Expression imm8)
// * - PUTINSTRREG8IMM8(String op, Reg8 reg8, Expression imm8)
// * - PUTINSTRREG16(String op, Reg16 reg16)
// * - PUTINSTRIMM16(String op, Expression imm16)
// * - PUTINSTRREG16IMM16(String op, Reg16 reg16, Expression imm16)
// *
// * All identifiers, opcodes, names, etc. are converted to upper case.
// *
// * I think there's not much difficulty understanding it if you are somewhat familiar with the 8085.
// * Otherwise, you can play around the PEG.js tester.
// *
// * TODO List
// * ---------
// * - Don't require a newline at the end of input (as a workaround, just parse programCode+"\n").
// * - Improve string parsing for DS directive (only alphanum + space supported ATM).
// */
//{
//  /* /-------------\
//   * | EXPRESSIONS |
//   * \-------------/ */
//  // TODO hack, but I need this outside the parser
//  window.UnaryOpExpression = function(op, expr) {
//    this.op = op;
//    this.expr = expr;
//  }
//  window.BinaryOpExpression = function(left, op, right) {
//    this.left = left;
//    this.op = op;
//    this.right = right;
//  }
//  window.ValueExpression = function(value) {
//    this.value = value;
//  }
//  window.IdentifierExpression = function(ident) {
//    this.ident = ident;
//  }
//}
//
//start
//  = blocks:block*
//     {
//       return [].concat.apply([], blocks); // Flatten array
//     }
//
//block
//  = __* "." __* "DEFINE"i __* comment? "\n" defs:definitionlist
//    {
//      return defs;
//    }
//  / __* "." __*"DATA"i __+ address:expression __* comment? "\n" decls:declarationlist
//    {
//      return [{ command: ".DATA", address: address }].concat(decls);
//    }
//  / __* "." __* "ORG"i __+ address:expression __* comment? "\n" instrs:instructionlist
//    {
//      return [{ command: ".ORG", address: address }].concat(instrs);
//    }
//  / __* comment? "\n"
//    {
//      return [];
//    }
//
//label
//  = name:identifier __* ":"
//      {
//        return { command: "DEFINELABEL", name: name };
//      }
//
//definitionlist
//  = deflist:(__* def:definition? __* comment? "\n" { return def != "" ? [def] : []; })*
//    {
//      return [].concat.apply([], deflist); // Flatten array
//    }
//definition
//  = name:identifier __+ value:expression
//     {
//       return { command: "DEFINECONSTANT", name: name, value: value };
//     }
//
//declarationlist
//  = decls:(__* label:label? __* decl:declaration? __* comment? "\n" { var a = []; if (label !== "") a.push(label); if (decl !== "") a.push(decl); return a; })*
//     {
//       return [].concat.apply([], decls); // Flatten array
//     }
//declaration
//  = kind:("DB"i / "DW"i) __+ first:expression next:(__* "," __* expr:expression { return expr; })*
//     {
//       return { command: kind.toUpperCase(), data: [first].concat(next) };
//     }
//  / kind:"DS"i __+ "\"" string:characterLiteral* "\""
//     {
//       return { command: kind.toUpperCase(), data: string.join("") };
//     }
//
//instructionlist
//  = instrs:(__* label:label? __* instr:instruction? __* comment? "\n" { var a = []; if (label !== "") a.push(label); if (instr !== "") a.push(instr); return a; })*
//     {
//       return [].concat.apply([], instrs); // Flatten array
//     }
//instruction
//  = op:("SPHL"i / "XTHL"i / "XCHG"i / "DAA"i / "STC"i / "RLC"i / "RRC"i / "RAL"i / "RAR"i / "CMA"i / "CMC"i / "PCHL"i / "RET"i / "RNZ"i / "RZ"i / "RNC"i / "RC"i / "RPO"i / "RPE"i / "RP"i / "RM"i / "NOP"i / "HLT"i / "DI"i / "EI"i / "RIM"i / "SIM"i)
//      {
//        return { command: "PUTINSTRNIL", op:op.toUpperCase() };
//      }
//  / op:"RST"i __+ imm3:expression
//      {
//        return { command: "PUTINSTRIMM3IMPLICIT", op:op.toUpperCase(), imm3:imm3 };
//      }
//  / op:("ADD"i / "ADC"i / "SUB"i / "SBB"i / "ANA"i / "XRA"i / "ORA"i / "CMP"i / "INR"i / "DCR"i) __+ reg8:reg8
//      {
//        return { command: "PUTINSTRREG8", op:op.toUpperCase(), reg8: reg8 };
//      }
//  / op:"MOV"i __+ dst8:reg8 __* "," __* src8:reg8
//      {
//        return { command: "PUTINSTRDST8SRC8", op:op.toUpperCase(), dst8: dst8, src8: src8 };
//      }
//  / op:("ADI"i / "ACI"i / "SUI"i / "SBI"i / "ANI"i / "XRI"i / "ORI"i / "CPI"i / "IN"i / "OUT"i) __+ imm8:expression
//      {
//        return { command: "PUTINSTRIMM8", op:op.toUpperCase(), imm8:imm8 };
//      }
//  / op:"MVI"i __+ reg8:reg8 __* "," __* imm8:expression
//      {
//        return { command: "PUTINSTRREG8IMM8", op:op.toUpperCase(), reg8:reg8, imm8: imm8 };
//      }
//  / op:("INX"i / "DCX"i / "DAD"i) __+ reg16:reg16_type1
//      {
//        return { command: "PUTINSTRREG16", op:op.toUpperCase(), reg16:reg16 };
//      }
//  / op:("PUSH"i / "POP"i) __+ reg16:reg16_type2
//      {
//        return { command: "PUTINSTRREG16", op:op.toUpperCase(), reg16:reg16 };
//      }
//  / op:("LDAX"i / "STAX"i) __+ reg16:reg16_type3
//      {
//        return { command: "PUTINSTRREG16", op:op.toUpperCase(), reg16:reg16 };
//      }
//  / op:("JMP"i / "JNZ"i / "JZ"i / "JNC"i / "JC"i / "JPO"i / "JPE"i / "JP"i / "JM"i / "CALL"i / "CNZ"i / "CZ"i / "CNC"i / "CC"i / "CPO"i / "CPE"i / "CP"i / "CM"i / "LDA"i / "STA"i / "LHLD"i / "SHLD"i) __+ imm16:expression
//      {
//        return { command: "PUTINSTRIMM16", op:op.toUpperCase(), imm16:imm16 };
//      }
//  / op:"LXI"i __+ reg16:reg16_type1 __* "," __* imm16:expression
//      {
//        return { command: "PUTINSTRREG16IMM16", op:op.toUpperCase(), reg16:reg16, imm16: imm16 };
//      }
//
//reg8
//  = reg8:("A"i / "B"i / "C"i / "D"i / "E"i / "H"i / "L"i / "M"i)
//      { return reg8.toUpperCase(); }
//reg16_type1
//  = reg16:("B"i / "D"i / "H"i / "SP"i)
//      { return reg16.toUpperCase(); }
//reg16_type2
//  = reg16:("B"i / "D"i / "H"i / "PSW"i)
//      { return reg16.toUpperCase(); }
//reg16_type3
//  = reg16:("B"i / "D"i)
//      { return reg16.toUpperCase(); }
//
//
//__
//  = [ \t]
//comment
//  = ";" [^\n]*
//
//
//expression = left:expressionstrong exprs:(weakop expressionstrong)*
//  {
//    var r = left;
//    for (var i = 0; i < exprs.length; i++)
//      r = new BinaryOpExpression(r, exprs[i][0], exprs[i][1]);
//    return r;
//  }
//expressionstrong = left:expressionelement exprs:(strongop expressionelement)*
//  {
//    var r = left;
//    for (var i = 0; i < exprs.length; i++)
//      r = new BinaryOpExpression(r, exprs[i][0], exprs[i][1]);
//    return r;
//  }
//expressionelement = signs:sign* value:(character / hexnumber / binarynumber / octalnumber / decnumber / identifier / ("(" expression ")"))
//  {
//    var r = value;
//    for (var i = 0; i < signs.length; i++)
//      r = new UnaryOpExpression(signs[i], r);
//    return r;
//  }
//weakop = ("+" / "-" / "&" / "|" / "^")
//strongop = ("*" / "/")
//sign = ("+" / "-")
//identifier = first:[A-Za-z_] next:[A-Za-z0-9_]*
//  { return new IdentifierExpression((first+next.join("")).toUpperCase()); }
//
//character = "'" character:characterLiteral "'"
//  { return new ValueExpression(character.charCodeAt(0)); }
//
//characterLiteral
//  = char:[ !#$%&()*+,-./0-9:;<=>?@A-Z\[\]^_`a-z{|}~]
//      { return char; }
// / '\\' 'n'
//      { return '\n'; }
// / '\\' 'r'
//      { return '\r'; }
// / '\\' 't'
//      { return '\t'; }
// / '\\' '\\'
//      { return '\\'; }
// / '\\' '\''
//      { return '\''; }
// / '\\' '\"'
//      { return '\"'; }
// / '\\' 'x' hexEscape:([0-7][0-9A-Fa-f])
//      { return String.fromCharCode(parseInt(hexEscape.join(""),16)); }
//
//hexnumber = digits:[0-9A-Fa-f]+ "H"i
//  { return new ValueExpression(parseInt(digits.join(""), 16)); }
//binarynumber = digits:[0-1]+ "B"i
//  { return new ValueExpression(parseInt(digits.join(""), 2)); }
//octalnumber = digits:[0-7]+ ("O"i / "Q"i)
//  { return new ValueExpression(parseInt(digits.join(""),8)); }
//decnumber = digits:[0-9]+ "D"i?
//  { return new ValueExpression(parseInt(digits.join(""),10)); }
parse8085 = (function(){
  /*
   * Generated by PEG.js 0.7.0.
   *
   * http://pegjs.majda.cz/
   */
  
  function quote(s) {
    /*
     * ECMA-262, 5th ed., 7.8.4: All characters may appear literally in a
     * string literal except for the closing quote character, backslash,
     * carriage return, line separator, paragraph separator, and line feed.
     * Any character may appear in the form of an escape sequence.
     *
     * For portability, we also escape escape all control and non-ASCII
     * characters. Note that "\0" and "\v" escape sequences are not used
     * because JSHint does not like the first and IE the second.
     */
     return '"' + s
      .replace(/\\/g, '\\\\')  // backslash
      .replace(/"/g, '\\"')    // closing quote character
      .replace(/\x08/g, '\\b') // backspace
      .replace(/\t/g, '\\t')   // horizontal tab
      .replace(/\n/g, '\\n')   // line feed
      .replace(/\f/g, '\\f')   // form feed
      .replace(/\r/g, '\\r')   // carriage return
      .replace(/[\x00-\x07\x0B\x0E-\x1F\x80-\uFFFF]/g, escape)
      + '"';
  }
  
  var result = {
    /*
     * Parses the input with a generated parser. If the parsing is successfull,
     * returns a value explicitly or implicitly specified by the grammar from
     * which the parser was generated (see |PEG.buildParser|). If the parsing is
     * unsuccessful, throws |PEG.parser.SyntaxError| describing the error.
     */
    parse: function(input, startRule) {
      var parseFunctions = {
        "start": parse_start,
        "block": parse_block,
        "label": parse_label,
        "definitionlist": parse_definitionlist,
        "definition": parse_definition,
        "declarationlist": parse_declarationlist,
        "declaration": parse_declaration,
        "instructionlist": parse_instructionlist,
        "instruction": parse_instruction,
        "reg8": parse_reg8,
        "reg16_type1": parse_reg16_type1,
        "reg16_type2": parse_reg16_type2,
        "reg16_type3": parse_reg16_type3,
        "__": parse___,
        "comment": parse_comment,
        "expression": parse_expression,
        "expressionstrong": parse_expressionstrong,
        "expressionelement": parse_expressionelement,
        "weakop": parse_weakop,
        "strongop": parse_strongop,
        "sign": parse_sign,
        "identifier": parse_identifier,
        "character": parse_character,
        "characterLiteral": parse_characterLiteral,
        "hexnumber": parse_hexnumber,
        "binarynumber": parse_binarynumber,
        "octalnumber": parse_octalnumber,
        "decnumber": parse_decnumber
      };
      
      if (startRule !== undefined) {
        if (parseFunctions[startRule] === undefined) {
          throw new Error("Invalid rule name: " + quote(startRule) + ".");
        }
      } else {
        startRule = "start";
      }
      
      var pos = 0;
      var reportFailures = 0;
      var rightmostFailuresPos = 0;
      var rightmostFailuresExpected = [];
      
      function padLeft(input, padding, length) {
        var result = input;
        
        var padLength = length - input.length;
        for (var i = 0; i < padLength; i++) {
          result = padding + result;
        }
        
        return result;
      }
      
      function escape(ch) {
        var charCode = ch.charCodeAt(0);
        var escapeChar;
        var length;
        
        if (charCode <= 0xFF) {
          escapeChar = 'x';
          length = 2;
        } else {
          escapeChar = 'u';
          length = 4;
        }
        
        return '\\' + escapeChar + padLeft(charCode.toString(16).toUpperCase(), '0', length);
      }
      
      function matchFailed(failure) {
        if (pos < rightmostFailuresPos) {
          return;
        }
        
        if (pos > rightmostFailuresPos) {
          rightmostFailuresPos = pos;
          rightmostFailuresExpected = [];
        }
        
        rightmostFailuresExpected.push(failure);
      }
      
      function parse_start() {
        var result0, result1;
        var pos0;
        
        pos0 = pos;
        result0 = [];
        result1 = parse_block();
        while (result1 !== null) {
          result0.push(result1);
          result1 = parse_block();
        }
        if (result0 !== null) {
          result0 = (function(offset, blocks) {
               return [].concat.apply([], blocks); // Flatten array
             })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_block() {
        var result0, result1, result2, result3, result4, result5, result6, result7, result8, result9;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        result0 = [];
        result1 = parse___();
        while (result1 !== null) {
          result0.push(result1);
          result1 = parse___();
        }
        if (result0 !== null) {
          if (input.charCodeAt(pos) === 46) {
            result1 = ".";
            pos++;
          } else {
            result1 = null;
            if (reportFailures === 0) {
              matchFailed("\".\"");
            }
          }
          if (result1 !== null) {
            result2 = [];
            result3 = parse___();
            while (result3 !== null) {
              result2.push(result3);
              result3 = parse___();
            }
            if (result2 !== null) {
              if (input.substr(pos, 6).toLowerCase() === "define") {
                result3 = input.substr(pos, 6);
                pos += 6;
              } else {
                result3 = null;
                if (reportFailures === 0) {
                  matchFailed("\"DEFINE\"");
                }
              }
              if (result3 !== null) {
                result4 = [];
                result5 = parse___();
                while (result5 !== null) {
                  result4.push(result5);
                  result5 = parse___();
                }
                if (result4 !== null) {
                  result5 = parse_comment();
                  result5 = result5 !== null ? result5 : "";
                  if (result5 !== null) {
                    if (input.charCodeAt(pos) === 10) {
                      result6 = "\n";
                      pos++;
                    } else {
                      result6 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"\\n\"");
                      }
                    }
                    if (result6 !== null) {
                      result7 = parse_definitionlist();
                      if (result7 !== null) {
                        result0 = [result0, result1, result2, result3, result4, result5, result6, result7];
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                } else {
                  result0 = null;
                  pos = pos1;
                }
              } else {
                result0 = null;
                pos = pos1;
              }
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, defs) {
              return defs;
            })(pos0, result0[7]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        if (result0 === null) {
          pos0 = pos;
          pos1 = pos;
          result0 = [];
          result1 = parse___();
          while (result1 !== null) {
            result0.push(result1);
            result1 = parse___();
          }
          if (result0 !== null) {
            if (input.charCodeAt(pos) === 46) {
              result1 = ".";
              pos++;
            } else {
              result1 = null;
              if (reportFailures === 0) {
                matchFailed("\".\"");
              }
            }
            if (result1 !== null) {
              result2 = [];
              result3 = parse___();
              while (result3 !== null) {
                result2.push(result3);
                result3 = parse___();
              }
              if (result2 !== null) {
                if (input.substr(pos, 4).toLowerCase() === "data") {
                  result3 = input.substr(pos, 4);
                  pos += 4;
                } else {
                  result3 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"DATA\"");
                  }
                }
                if (result3 !== null) {
                  result5 = parse___();
                  if (result5 !== null) {
                    result4 = [];
                    while (result5 !== null) {
                      result4.push(result5);
                      result5 = parse___();
                    }
                  } else {
                    result4 = null;
                  }
                  if (result4 !== null) {
                    result5 = parse_expression();
                    if (result5 !== null) {
                      result6 = [];
                      result7 = parse___();
                      while (result7 !== null) {
                        result6.push(result7);
                        result7 = parse___();
                      }
                      if (result6 !== null) {
                        result7 = parse_comment();
                        result7 = result7 !== null ? result7 : "";
                        if (result7 !== null) {
                          if (input.charCodeAt(pos) === 10) {
                            result8 = "\n";
                            pos++;
                          } else {
                            result8 = null;
                            if (reportFailures === 0) {
                              matchFailed("\"\\n\"");
                            }
                          }
                          if (result8 !== null) {
                            result9 = parse_declarationlist();
                            if (result9 !== null) {
                              result0 = [result0, result1, result2, result3, result4, result5, result6, result7, result8, result9];
                            } else {
                              result0 = null;
                              pos = pos1;
                            }
                          } else {
                            result0 = null;
                            pos = pos1;
                          }
                        } else {
                          result0 = null;
                          pos = pos1;
                        }
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                } else {
                  result0 = null;
                  pos = pos1;
                }
              } else {
                result0 = null;
                pos = pos1;
              }
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
          if (result0 !== null) {
            result0 = (function(offset, address, decls) {
                return [{ command: ".DATA", address: address }].concat(decls);
              })(pos0, result0[5], result0[9]);
          }
          if (result0 === null) {
            pos = pos0;
          }
          if (result0 === null) {
            pos0 = pos;
            pos1 = pos;
            result0 = [];
            result1 = parse___();
            while (result1 !== null) {
              result0.push(result1);
              result1 = parse___();
            }
            if (result0 !== null) {
              if (input.charCodeAt(pos) === 46) {
                result1 = ".";
                pos++;
              } else {
                result1 = null;
                if (reportFailures === 0) {
                  matchFailed("\".\"");
                }
              }
              if (result1 !== null) {
                result2 = [];
                result3 = parse___();
                while (result3 !== null) {
                  result2.push(result3);
                  result3 = parse___();
                }
                if (result2 !== null) {
                  if (input.substr(pos, 3).toLowerCase() === "org") {
                    result3 = input.substr(pos, 3);
                    pos += 3;
                  } else {
                    result3 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"ORG\"");
                    }
                  }
                  if (result3 !== null) {
                    result5 = parse___();
                    if (result5 !== null) {
                      result4 = [];
                      while (result5 !== null) {
                        result4.push(result5);
                        result5 = parse___();
                      }
                    } else {
                      result4 = null;
                    }
                    if (result4 !== null) {
                      result5 = parse_expression();
                      if (result5 !== null) {
                        result6 = [];
                        result7 = parse___();
                        while (result7 !== null) {
                          result6.push(result7);
                          result7 = parse___();
                        }
                        if (result6 !== null) {
                          result7 = parse_comment();
                          result7 = result7 !== null ? result7 : "";
                          if (result7 !== null) {
                            if (input.charCodeAt(pos) === 10) {
                              result8 = "\n";
                              pos++;
                            } else {
                              result8 = null;
                              if (reportFailures === 0) {
                                matchFailed("\"\\n\"");
                              }
                            }
                            if (result8 !== null) {
                              result9 = parse_instructionlist();
                              if (result9 !== null) {
                                result0 = [result0, result1, result2, result3, result4, result5, result6, result7, result8, result9];
                              } else {
                                result0 = null;
                                pos = pos1;
                              }
                            } else {
                              result0 = null;
                              pos = pos1;
                            }
                          } else {
                            result0 = null;
                            pos = pos1;
                          }
                        } else {
                          result0 = null;
                          pos = pos1;
                        }
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                } else {
                  result0 = null;
                  pos = pos1;
                }
              } else {
                result0 = null;
                pos = pos1;
              }
            } else {
              result0 = null;
              pos = pos1;
            }
            if (result0 !== null) {
              result0 = (function(offset, address, instrs) {
                  return [{ command: ".ORG", address: address }].concat(instrs);
                })(pos0, result0[5], result0[9]);
            }
            if (result0 === null) {
              pos = pos0;
            }
            if (result0 === null) {
              pos0 = pos;
              pos1 = pos;
              result0 = [];
              result1 = parse___();
              while (result1 !== null) {
                result0.push(result1);
                result1 = parse___();
              }
              if (result0 !== null) {
                result1 = parse_comment();
                result1 = result1 !== null ? result1 : "";
                if (result1 !== null) {
                  if (input.charCodeAt(pos) === 10) {
                    result2 = "\n";
                    pos++;
                  } else {
                    result2 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"\\n\"");
                    }
                  }
                  if (result2 !== null) {
                    result0 = [result0, result1, result2];
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                } else {
                  result0 = null;
                  pos = pos1;
                }
              } else {
                result0 = null;
                pos = pos1;
              }
              if (result0 !== null) {
                result0 = (function(offset) {
                    return [];
                  })(pos0);
              }
              if (result0 === null) {
                pos = pos0;
              }
            }
          }
        }
        return result0;
      }
      
      function parse_label() {
        var result0, result1, result2;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        result0 = parse_identifier();
        if (result0 !== null) {
          result1 = [];
          result2 = parse___();
          while (result2 !== null) {
            result1.push(result2);
            result2 = parse___();
          }
          if (result1 !== null) {
            if (input.charCodeAt(pos) === 58) {
              result2 = ":";
              pos++;
            } else {
              result2 = null;
              if (reportFailures === 0) {
                matchFailed("\":\"");
              }
            }
            if (result2 !== null) {
              result0 = [result0, result1, result2];
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, name) {
                return { command: "DEFINELABEL", name: name };
              })(pos0, result0[0]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_definitionlist() {
        var result0, result1, result2, result3, result4, result5;
        var pos0, pos1, pos2;
        
        pos0 = pos;
        result0 = [];
        pos1 = pos;
        pos2 = pos;
        result1 = [];
        result2 = parse___();
        while (result2 !== null) {
          result1.push(result2);
          result2 = parse___();
        }
        if (result1 !== null) {
          result2 = parse_definition();
          result2 = result2 !== null ? result2 : "";
          if (result2 !== null) {
            result3 = [];
            result4 = parse___();
            while (result4 !== null) {
              result3.push(result4);
              result4 = parse___();
            }
            if (result3 !== null) {
              result4 = parse_comment();
              result4 = result4 !== null ? result4 : "";
              if (result4 !== null) {
                if (input.charCodeAt(pos) === 10) {
                  result5 = "\n";
                  pos++;
                } else {
                  result5 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"\\n\"");
                  }
                }
                if (result5 !== null) {
                  result1 = [result1, result2, result3, result4, result5];
                } else {
                  result1 = null;
                  pos = pos2;
                }
              } else {
                result1 = null;
                pos = pos2;
              }
            } else {
              result1 = null;
              pos = pos2;
            }
          } else {
            result1 = null;
            pos = pos2;
          }
        } else {
          result1 = null;
          pos = pos2;
        }
        if (result1 !== null) {
          result1 = (function(offset, def) { return def != "" ? [def] : []; })(pos1, result1[1]);
        }
        if (result1 === null) {
          pos = pos1;
        }
        while (result1 !== null) {
          result0.push(result1);
          pos1 = pos;
          pos2 = pos;
          result1 = [];
          result2 = parse___();
          while (result2 !== null) {
            result1.push(result2);
            result2 = parse___();
          }
          if (result1 !== null) {
            result2 = parse_definition();
            result2 = result2 !== null ? result2 : "";
            if (result2 !== null) {
              result3 = [];
              result4 = parse___();
              while (result4 !== null) {
                result3.push(result4);
                result4 = parse___();
              }
              if (result3 !== null) {
                result4 = parse_comment();
                result4 = result4 !== null ? result4 : "";
                if (result4 !== null) {
                  if (input.charCodeAt(pos) === 10) {
                    result5 = "\n";
                    pos++;
                  } else {
                    result5 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"\\n\"");
                    }
                  }
                  if (result5 !== null) {
                    result1 = [result1, result2, result3, result4, result5];
                  } else {
                    result1 = null;
                    pos = pos2;
                  }
                } else {
                  result1 = null;
                  pos = pos2;
                }
              } else {
                result1 = null;
                pos = pos2;
              }
            } else {
              result1 = null;
              pos = pos2;
            }
          } else {
            result1 = null;
            pos = pos2;
          }
          if (result1 !== null) {
            result1 = (function(offset, def) { return def != "" ? [def] : []; })(pos1, result1[1]);
          }
          if (result1 === null) {
            pos = pos1;
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, deflist) {
              return [].concat.apply([], deflist); // Flatten array
            })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_definition() {
        var result0, result1, result2;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        result0 = parse_identifier();
        if (result0 !== null) {
          result2 = parse___();
          if (result2 !== null) {
            result1 = [];
            while (result2 !== null) {
              result1.push(result2);
              result2 = parse___();
            }
          } else {
            result1 = null;
          }
          if (result1 !== null) {
            result2 = parse_expression();
            if (result2 !== null) {
              result0 = [result0, result1, result2];
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, name, value) {
               return { command: "DEFINECONSTANT", name: name, value: value };
             })(pos0, result0[0], result0[2]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_declarationlist() {
        var result0, result1, result2, result3, result4, result5, result6, result7;
        var pos0, pos1, pos2;
        
        pos0 = pos;
        result0 = [];
        pos1 = pos;
        pos2 = pos;
        result1 = [];
        result2 = parse___();
        while (result2 !== null) {
          result1.push(result2);
          result2 = parse___();
        }
        if (result1 !== null) {
          result2 = parse_label();
          result2 = result2 !== null ? result2 : "";
          if (result2 !== null) {
            result3 = [];
            result4 = parse___();
            while (result4 !== null) {
              result3.push(result4);
              result4 = parse___();
            }
            if (result3 !== null) {
              result4 = parse_declaration();
              result4 = result4 !== null ? result4 : "";
              if (result4 !== null) {
                result5 = [];
                result6 = parse___();
                while (result6 !== null) {
                  result5.push(result6);
                  result6 = parse___();
                }
                if (result5 !== null) {
                  result6 = parse_comment();
                  result6 = result6 !== null ? result6 : "";
                  if (result6 !== null) {
                    if (input.charCodeAt(pos) === 10) {
                      result7 = "\n";
                      pos++;
                    } else {
                      result7 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"\\n\"");
                      }
                    }
                    if (result7 !== null) {
                      result1 = [result1, result2, result3, result4, result5, result6, result7];
                    } else {
                      result1 = null;
                      pos = pos2;
                    }
                  } else {
                    result1 = null;
                    pos = pos2;
                  }
                } else {
                  result1 = null;
                  pos = pos2;
                }
              } else {
                result1 = null;
                pos = pos2;
              }
            } else {
              result1 = null;
              pos = pos2;
            }
          } else {
            result1 = null;
            pos = pos2;
          }
        } else {
          result1 = null;
          pos = pos2;
        }
        if (result1 !== null) {
          result1 = (function(offset, label, decl) { var a = []; if (label !== "") a.push(label); if (decl !== "") a.push(decl); return a; })(pos1, result1[1], result1[3]);
        }
        if (result1 === null) {
          pos = pos1;
        }
        while (result1 !== null) {
          result0.push(result1);
          pos1 = pos;
          pos2 = pos;
          result1 = [];
          result2 = parse___();
          while (result2 !== null) {
            result1.push(result2);
            result2 = parse___();
          }
          if (result1 !== null) {
            result2 = parse_label();
            result2 = result2 !== null ? result2 : "";
            if (result2 !== null) {
              result3 = [];
              result4 = parse___();
              while (result4 !== null) {
                result3.push(result4);
                result4 = parse___();
              }
              if (result3 !== null) {
                result4 = parse_declaration();
                result4 = result4 !== null ? result4 : "";
                if (result4 !== null) {
                  result5 = [];
                  result6 = parse___();
                  while (result6 !== null) {
                    result5.push(result6);
                    result6 = parse___();
                  }
                  if (result5 !== null) {
                    result6 = parse_comment();
                    result6 = result6 !== null ? result6 : "";
                    if (result6 !== null) {
                      if (input.charCodeAt(pos) === 10) {
                        result7 = "\n";
                        pos++;
                      } else {
                        result7 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"\\n\"");
                        }
                      }
                      if (result7 !== null) {
                        result1 = [result1, result2, result3, result4, result5, result6, result7];
                      } else {
                        result1 = null;
                        pos = pos2;
                      }
                    } else {
                      result1 = null;
                      pos = pos2;
                    }
                  } else {
                    result1 = null;
                    pos = pos2;
                  }
                } else {
                  result1 = null;
                  pos = pos2;
                }
              } else {
                result1 = null;
                pos = pos2;
              }
            } else {
              result1 = null;
              pos = pos2;
            }
          } else {
            result1 = null;
            pos = pos2;
          }
          if (result1 !== null) {
            result1 = (function(offset, label, decl) { var a = []; if (label !== "") a.push(label); if (decl !== "") a.push(decl); return a; })(pos1, result1[1], result1[3]);
          }
          if (result1 === null) {
            pos = pos1;
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, decls) {
               return [].concat.apply([], decls); // Flatten array
             })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_declaration() {
        var result0, result1, result2, result3, result4, result5, result6, result7;
        var pos0, pos1, pos2, pos3;
        
        pos0 = pos;
        pos1 = pos;
        if (input.substr(pos, 2).toLowerCase() === "db") {
          result0 = input.substr(pos, 2);
          pos += 2;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"DB\"");
          }
        }
        if (result0 === null) {
          if (input.substr(pos, 2).toLowerCase() === "dw") {
            result0 = input.substr(pos, 2);
            pos += 2;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"DW\"");
            }
          }
        }
        if (result0 !== null) {
          result2 = parse___();
          if (result2 !== null) {
            result1 = [];
            while (result2 !== null) {
              result1.push(result2);
              result2 = parse___();
            }
          } else {
            result1 = null;
          }
          if (result1 !== null) {
            result2 = parse_expression();
            if (result2 !== null) {
              result3 = [];
              pos2 = pos;
              pos3 = pos;
              result4 = [];
              result5 = parse___();
              while (result5 !== null) {
                result4.push(result5);
                result5 = parse___();
              }
              if (result4 !== null) {
                if (input.charCodeAt(pos) === 44) {
                  result5 = ",";
                  pos++;
                } else {
                  result5 = null;
                  if (reportFailures === 0) {
                    matchFailed("\",\"");
                  }
                }
                if (result5 !== null) {
                  result6 = [];
                  result7 = parse___();
                  while (result7 !== null) {
                    result6.push(result7);
                    result7 = parse___();
                  }
                  if (result6 !== null) {
                    result7 = parse_expression();
                    if (result7 !== null) {
                      result4 = [result4, result5, result6, result7];
                    } else {
                      result4 = null;
                      pos = pos3;
                    }
                  } else {
                    result4 = null;
                    pos = pos3;
                  }
                } else {
                  result4 = null;
                  pos = pos3;
                }
              } else {
                result4 = null;
                pos = pos3;
              }
              if (result4 !== null) {
                result4 = (function(offset, expr) { return expr; })(pos2, result4[3]);
              }
              if (result4 === null) {
                pos = pos2;
              }
              while (result4 !== null) {
                result3.push(result4);
                pos2 = pos;
                pos3 = pos;
                result4 = [];
                result5 = parse___();
                while (result5 !== null) {
                  result4.push(result5);
                  result5 = parse___();
                }
                if (result4 !== null) {
                  if (input.charCodeAt(pos) === 44) {
                    result5 = ",";
                    pos++;
                  } else {
                    result5 = null;
                    if (reportFailures === 0) {
                      matchFailed("\",\"");
                    }
                  }
                  if (result5 !== null) {
                    result6 = [];
                    result7 = parse___();
                    while (result7 !== null) {
                      result6.push(result7);
                      result7 = parse___();
                    }
                    if (result6 !== null) {
                      result7 = parse_expression();
                      if (result7 !== null) {
                        result4 = [result4, result5, result6, result7];
                      } else {
                        result4 = null;
                        pos = pos3;
                      }
                    } else {
                      result4 = null;
                      pos = pos3;
                    }
                  } else {
                    result4 = null;
                    pos = pos3;
                  }
                } else {
                  result4 = null;
                  pos = pos3;
                }
                if (result4 !== null) {
                  result4 = (function(offset, expr) { return expr; })(pos2, result4[3]);
                }
                if (result4 === null) {
                  pos = pos2;
                }
              }
              if (result3 !== null) {
                result0 = [result0, result1, result2, result3];
              } else {
                result0 = null;
                pos = pos1;
              }
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, kind, first, next) {
               return { command: kind.toUpperCase(), data: [first].concat(next) };
             })(pos0, result0[0], result0[2], result0[3]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        if (result0 === null) {
          pos0 = pos;
          pos1 = pos;
          if (input.substr(pos, 2).toLowerCase() === "ds") {
            result0 = input.substr(pos, 2);
            pos += 2;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"DS\"");
            }
          }
          if (result0 !== null) {
            result2 = parse___();
            if (result2 !== null) {
              result1 = [];
              while (result2 !== null) {
                result1.push(result2);
                result2 = parse___();
              }
            } else {
              result1 = null;
            }
            if (result1 !== null) {
              if (input.charCodeAt(pos) === 34) {
                result2 = "\"";
                pos++;
              } else {
                result2 = null;
                if (reportFailures === 0) {
                  matchFailed("\"\\\"\"");
                }
              }
              if (result2 !== null) {
                result3 = [];
                result4 = parse_characterLiteral();
                while (result4 !== null) {
                  result3.push(result4);
                  result4 = parse_characterLiteral();
                }
                if (result3 !== null) {
                  if (input.charCodeAt(pos) === 34) {
                    result4 = "\"";
                    pos++;
                  } else {
                    result4 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"\\\"\"");
                    }
                  }
                  if (result4 !== null) {
                    result0 = [result0, result1, result2, result3, result4];
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                } else {
                  result0 = null;
                  pos = pos1;
                }
              } else {
                result0 = null;
                pos = pos1;
              }
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
          if (result0 !== null) {
            result0 = (function(offset, kind, string) {
                 return { command: kind.toUpperCase(), data: string.join("") };
               })(pos0, result0[0], result0[3]);
          }
          if (result0 === null) {
            pos = pos0;
          }
        }
        return result0;
      }
      
      function parse_instructionlist() {
        var result0, result1, result2, result3, result4, result5, result6, result7;
        var pos0, pos1, pos2;
        
        pos0 = pos;
        result0 = [];
        pos1 = pos;
        pos2 = pos;
        result1 = [];
        result2 = parse___();
        while (result2 !== null) {
          result1.push(result2);
          result2 = parse___();
        }
        if (result1 !== null) {
          result2 = parse_label();
          result2 = result2 !== null ? result2 : "";
          if (result2 !== null) {
            result3 = [];
            result4 = parse___();
            while (result4 !== null) {
              result3.push(result4);
              result4 = parse___();
            }
            if (result3 !== null) {
              result4 = parse_instruction();
              result4 = result4 !== null ? result4 : "";
              if (result4 !== null) {
                result5 = [];
                result6 = parse___();
                while (result6 !== null) {
                  result5.push(result6);
                  result6 = parse___();
                }
                if (result5 !== null) {
                  result6 = parse_comment();
                  result6 = result6 !== null ? result6 : "";
                  if (result6 !== null) {
                    if (input.charCodeAt(pos) === 10) {
                      result7 = "\n";
                      pos++;
                    } else {
                      result7 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"\\n\"");
                      }
                    }
                    if (result7 !== null) {
                      result1 = [result1, result2, result3, result4, result5, result6, result7];
                    } else {
                      result1 = null;
                      pos = pos2;
                    }
                  } else {
                    result1 = null;
                    pos = pos2;
                  }
                } else {
                  result1 = null;
                  pos = pos2;
                }
              } else {
                result1 = null;
                pos = pos2;
              }
            } else {
              result1 = null;
              pos = pos2;
            }
          } else {
            result1 = null;
            pos = pos2;
          }
        } else {
          result1 = null;
          pos = pos2;
        }
        if (result1 !== null) {
          result1 = (function(offset, label, instr) { var a = []; if (label !== "") a.push(label); if (instr !== "") a.push(instr); return a; })(pos1, result1[1], result1[3]);
        }
        if (result1 === null) {
          pos = pos1;
        }
        while (result1 !== null) {
          result0.push(result1);
          pos1 = pos;
          pos2 = pos;
          result1 = [];
          result2 = parse___();
          while (result2 !== null) {
            result1.push(result2);
            result2 = parse___();
          }
          if (result1 !== null) {
            result2 = parse_label();
            result2 = result2 !== null ? result2 : "";
            if (result2 !== null) {
              result3 = [];
              result4 = parse___();
              while (result4 !== null) {
                result3.push(result4);
                result4 = parse___();
              }
              if (result3 !== null) {
                result4 = parse_instruction();
                result4 = result4 !== null ? result4 : "";
                if (result4 !== null) {
                  result5 = [];
                  result6 = parse___();
                  while (result6 !== null) {
                    result5.push(result6);
                    result6 = parse___();
                  }
                  if (result5 !== null) {
                    result6 = parse_comment();
                    result6 = result6 !== null ? result6 : "";
                    if (result6 !== null) {
                      if (input.charCodeAt(pos) === 10) {
                        result7 = "\n";
                        pos++;
                      } else {
                        result7 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"\\n\"");
                        }
                      }
                      if (result7 !== null) {
                        result1 = [result1, result2, result3, result4, result5, result6, result7];
                      } else {
                        result1 = null;
                        pos = pos2;
                      }
                    } else {
                      result1 = null;
                      pos = pos2;
                    }
                  } else {
                    result1 = null;
                    pos = pos2;
                  }
                } else {
                  result1 = null;
                  pos = pos2;
                }
              } else {
                result1 = null;
                pos = pos2;
              }
            } else {
              result1 = null;
              pos = pos2;
            }
          } else {
            result1 = null;
            pos = pos2;
          }
          if (result1 !== null) {
            result1 = (function(offset, label, instr) { var a = []; if (label !== "") a.push(label); if (instr !== "") a.push(instr); return a; })(pos1, result1[1], result1[3]);
          }
          if (result1 === null) {
            pos = pos1;
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, instrs) {
               return [].concat.apply([], instrs); // Flatten array
             })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_instruction() {
        var result0, result1, result2, result3, result4, result5, result6;
        var pos0, pos1;
        
        pos0 = pos;
        if (input.substr(pos, 4).toLowerCase() === "sphl") {
          result0 = input.substr(pos, 4);
          pos += 4;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"SPHL\"");
          }
        }
        if (result0 === null) {
          if (input.substr(pos, 4).toLowerCase() === "xthl") {
            result0 = input.substr(pos, 4);
            pos += 4;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"XTHL\"");
            }
          }
          if (result0 === null) {
            if (input.substr(pos, 4).toLowerCase() === "xchg") {
              result0 = input.substr(pos, 4);
              pos += 4;
            } else {
              result0 = null;
              if (reportFailures === 0) {
                matchFailed("\"XCHG\"");
              }
            }
            if (result0 === null) {
              if (input.substr(pos, 3).toLowerCase() === "daa") {
                result0 = input.substr(pos, 3);
                pos += 3;
              } else {
                result0 = null;
                if (reportFailures === 0) {
                  matchFailed("\"DAA\"");
                }
              }
              if (result0 === null) {
                if (input.substr(pos, 3).toLowerCase() === "stc") {
                  result0 = input.substr(pos, 3);
                  pos += 3;
                } else {
                  result0 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"STC\"");
                  }
                }
                if (result0 === null) {
                  if (input.substr(pos, 3).toLowerCase() === "rlc") {
                    result0 = input.substr(pos, 3);
                    pos += 3;
                  } else {
                    result0 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"RLC\"");
                    }
                  }
                  if (result0 === null) {
                    if (input.substr(pos, 3).toLowerCase() === "rrc") {
                      result0 = input.substr(pos, 3);
                      pos += 3;
                    } else {
                      result0 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"RRC\"");
                      }
                    }
                    if (result0 === null) {
                      if (input.substr(pos, 3).toLowerCase() === "ral") {
                        result0 = input.substr(pos, 3);
                        pos += 3;
                      } else {
                        result0 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"RAL\"");
                        }
                      }
                      if (result0 === null) {
                        if (input.substr(pos, 3).toLowerCase() === "rar") {
                          result0 = input.substr(pos, 3);
                          pos += 3;
                        } else {
                          result0 = null;
                          if (reportFailures === 0) {
                            matchFailed("\"RAR\"");
                          }
                        }
                        if (result0 === null) {
                          if (input.substr(pos, 3).toLowerCase() === "cma") {
                            result0 = input.substr(pos, 3);
                            pos += 3;
                          } else {
                            result0 = null;
                            if (reportFailures === 0) {
                              matchFailed("\"CMA\"");
                            }
                          }
                          if (result0 === null) {
                            if (input.substr(pos, 3).toLowerCase() === "cmc") {
                              result0 = input.substr(pos, 3);
                              pos += 3;
                            } else {
                              result0 = null;
                              if (reportFailures === 0) {
                                matchFailed("\"CMC\"");
                              }
                            }
                            if (result0 === null) {
                              if (input.substr(pos, 4).toLowerCase() === "pchl") {
                                result0 = input.substr(pos, 4);
                                pos += 4;
                              } else {
                                result0 = null;
                                if (reportFailures === 0) {
                                  matchFailed("\"PCHL\"");
                                }
                              }
                              if (result0 === null) {
                                if (input.substr(pos, 3).toLowerCase() === "ret") {
                                  result0 = input.substr(pos, 3);
                                  pos += 3;
                                } else {
                                  result0 = null;
                                  if (reportFailures === 0) {
                                    matchFailed("\"RET\"");
                                  }
                                }
                                if (result0 === null) {
                                  if (input.substr(pos, 3).toLowerCase() === "rnz") {
                                    result0 = input.substr(pos, 3);
                                    pos += 3;
                                  } else {
                                    result0 = null;
                                    if (reportFailures === 0) {
                                      matchFailed("\"RNZ\"");
                                    }
                                  }
                                  if (result0 === null) {
                                    if (input.substr(pos, 2).toLowerCase() === "rz") {
                                      result0 = input.substr(pos, 2);
                                      pos += 2;
                                    } else {
                                      result0 = null;
                                      if (reportFailures === 0) {
                                        matchFailed("\"RZ\"");
                                      }
                                    }
                                    if (result0 === null) {
                                      if (input.substr(pos, 3).toLowerCase() === "rnc") {
                                        result0 = input.substr(pos, 3);
                                        pos += 3;
                                      } else {
                                        result0 = null;
                                        if (reportFailures === 0) {
                                          matchFailed("\"RNC\"");
                                        }
                                      }
                                      if (result0 === null) {
                                        if (input.substr(pos, 2).toLowerCase() === "rc") {
                                          result0 = input.substr(pos, 2);
                                          pos += 2;
                                        } else {
                                          result0 = null;
                                          if (reportFailures === 0) {
                                            matchFailed("\"RC\"");
                                          }
                                        }
                                        if (result0 === null) {
                                          if (input.substr(pos, 3).toLowerCase() === "rpo") {
                                            result0 = input.substr(pos, 3);
                                            pos += 3;
                                          } else {
                                            result0 = null;
                                            if (reportFailures === 0) {
                                              matchFailed("\"RPO\"");
                                            }
                                          }
                                          if (result0 === null) {
                                            if (input.substr(pos, 3).toLowerCase() === "rpe") {
                                              result0 = input.substr(pos, 3);
                                              pos += 3;
                                            } else {
                                              result0 = null;
                                              if (reportFailures === 0) {
                                                matchFailed("\"RPE\"");
                                              }
                                            }
                                            if (result0 === null) {
                                              if (input.substr(pos, 2).toLowerCase() === "rp") {
                                                result0 = input.substr(pos, 2);
                                                pos += 2;
                                              } else {
                                                result0 = null;
                                                if (reportFailures === 0) {
                                                  matchFailed("\"RP\"");
                                                }
                                              }
                                              if (result0 === null) {
                                                if (input.substr(pos, 2).toLowerCase() === "rm") {
                                                  result0 = input.substr(pos, 2);
                                                  pos += 2;
                                                } else {
                                                  result0 = null;
                                                  if (reportFailures === 0) {
                                                    matchFailed("\"RM\"");
                                                  }
                                                }
                                                if (result0 === null) {
                                                  if (input.substr(pos, 3).toLowerCase() === "nop") {
                                                    result0 = input.substr(pos, 3);
                                                    pos += 3;
                                                  } else {
                                                    result0 = null;
                                                    if (reportFailures === 0) {
                                                      matchFailed("\"NOP\"");
                                                    }
                                                  }
                                                  if (result0 === null) {
                                                    if (input.substr(pos, 3).toLowerCase() === "hlt") {
                                                      result0 = input.substr(pos, 3);
                                                      pos += 3;
                                                    } else {
                                                      result0 = null;
                                                      if (reportFailures === 0) {
                                                        matchFailed("\"HLT\"");
                                                      }
                                                    }
                                                    if (result0 === null) {
                                                      if (input.substr(pos, 2).toLowerCase() === "di") {
                                                        result0 = input.substr(pos, 2);
                                                        pos += 2;
                                                      } else {
                                                        result0 = null;
                                                        if (reportFailures === 0) {
                                                          matchFailed("\"DI\"");
                                                        }
                                                      }
                                                      if (result0 === null) {
                                                        if (input.substr(pos, 2).toLowerCase() === "ei") {
                                                          result0 = input.substr(pos, 2);
                                                          pos += 2;
                                                        } else {
                                                          result0 = null;
                                                          if (reportFailures === 0) {
                                                            matchFailed("\"EI\"");
                                                          }
                                                        }
                                                        if (result0 === null) {
                                                          if (input.substr(pos, 3).toLowerCase() === "rim") {
                                                            result0 = input.substr(pos, 3);
                                                            pos += 3;
                                                          } else {
                                                            result0 = null;
                                                            if (reportFailures === 0) {
                                                              matchFailed("\"RIM\"");
                                                            }
                                                          }
                                                          if (result0 === null) {
                                                            if (input.substr(pos, 3).toLowerCase() === "sim") {
                                                              result0 = input.substr(pos, 3);
                                                              pos += 3;
                                                            } else {
                                                              result0 = null;
                                                              if (reportFailures === 0) {
                                                                matchFailed("\"SIM\"");
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, op) {
                return { command: "PUTINSTRNIL", op:op.toUpperCase() };
              })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        if (result0 === null) {
          pos0 = pos;
          pos1 = pos;
          if (input.substr(pos, 3).toLowerCase() === "rst") {
            result0 = input.substr(pos, 3);
            pos += 3;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"RST\"");
            }
          }
          if (result0 !== null) {
            result2 = parse___();
            if (result2 !== null) {
              result1 = [];
              while (result2 !== null) {
                result1.push(result2);
                result2 = parse___();
              }
            } else {
              result1 = null;
            }
            if (result1 !== null) {
              result2 = parse_expression();
              if (result2 !== null) {
                result0 = [result0, result1, result2];
              } else {
                result0 = null;
                pos = pos1;
              }
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
          if (result0 !== null) {
            result0 = (function(offset, op, imm3) {
                  return { command: "PUTINSTRIMM3IMPLICIT", op:op.toUpperCase(), imm3:imm3 };
                })(pos0, result0[0], result0[2]);
          }
          if (result0 === null) {
            pos = pos0;
          }
          if (result0 === null) {
            pos0 = pos;
            pos1 = pos;
            if (input.substr(pos, 3).toLowerCase() === "add") {
              result0 = input.substr(pos, 3);
              pos += 3;
            } else {
              result0 = null;
              if (reportFailures === 0) {
                matchFailed("\"ADD\"");
              }
            }
            if (result0 === null) {
              if (input.substr(pos, 3).toLowerCase() === "adc") {
                result0 = input.substr(pos, 3);
                pos += 3;
              } else {
                result0 = null;
                if (reportFailures === 0) {
                  matchFailed("\"ADC\"");
                }
              }
              if (result0 === null) {
                if (input.substr(pos, 3).toLowerCase() === "sub") {
                  result0 = input.substr(pos, 3);
                  pos += 3;
                } else {
                  result0 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"SUB\"");
                  }
                }
                if (result0 === null) {
                  if (input.substr(pos, 3).toLowerCase() === "sbb") {
                    result0 = input.substr(pos, 3);
                    pos += 3;
                  } else {
                    result0 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"SBB\"");
                    }
                  }
                  if (result0 === null) {
                    if (input.substr(pos, 3).toLowerCase() === "ana") {
                      result0 = input.substr(pos, 3);
                      pos += 3;
                    } else {
                      result0 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"ANA\"");
                      }
                    }
                    if (result0 === null) {
                      if (input.substr(pos, 3).toLowerCase() === "xra") {
                        result0 = input.substr(pos, 3);
                        pos += 3;
                      } else {
                        result0 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"XRA\"");
                        }
                      }
                      if (result0 === null) {
                        if (input.substr(pos, 3).toLowerCase() === "ora") {
                          result0 = input.substr(pos, 3);
                          pos += 3;
                        } else {
                          result0 = null;
                          if (reportFailures === 0) {
                            matchFailed("\"ORA\"");
                          }
                        }
                        if (result0 === null) {
                          if (input.substr(pos, 3).toLowerCase() === "cmp") {
                            result0 = input.substr(pos, 3);
                            pos += 3;
                          } else {
                            result0 = null;
                            if (reportFailures === 0) {
                              matchFailed("\"CMP\"");
                            }
                          }
                          if (result0 === null) {
                            if (input.substr(pos, 3).toLowerCase() === "inr") {
                              result0 = input.substr(pos, 3);
                              pos += 3;
                            } else {
                              result0 = null;
                              if (reportFailures === 0) {
                                matchFailed("\"INR\"");
                              }
                            }
                            if (result0 === null) {
                              if (input.substr(pos, 3).toLowerCase() === "dcr") {
                                result0 = input.substr(pos, 3);
                                pos += 3;
                              } else {
                                result0 = null;
                                if (reportFailures === 0) {
                                  matchFailed("\"DCR\"");
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            if (result0 !== null) {
              result2 = parse___();
              if (result2 !== null) {
                result1 = [];
                while (result2 !== null) {
                  result1.push(result2);
                  result2 = parse___();
                }
              } else {
                result1 = null;
              }
              if (result1 !== null) {
                result2 = parse_reg8();
                if (result2 !== null) {
                  result0 = [result0, result1, result2];
                } else {
                  result0 = null;
                  pos = pos1;
                }
              } else {
                result0 = null;
                pos = pos1;
              }
            } else {
              result0 = null;
              pos = pos1;
            }
            if (result0 !== null) {
              result0 = (function(offset, op, reg8) {
                    return { command: "PUTINSTRREG8", op:op.toUpperCase(), reg8: reg8 };
                  })(pos0, result0[0], result0[2]);
            }
            if (result0 === null) {
              pos = pos0;
            }
            if (result0 === null) {
              pos0 = pos;
              pos1 = pos;
              if (input.substr(pos, 3).toLowerCase() === "mov") {
                result0 = input.substr(pos, 3);
                pos += 3;
              } else {
                result0 = null;
                if (reportFailures === 0) {
                  matchFailed("\"MOV\"");
                }
              }
              if (result0 !== null) {
                result2 = parse___();
                if (result2 !== null) {
                  result1 = [];
                  while (result2 !== null) {
                    result1.push(result2);
                    result2 = parse___();
                  }
                } else {
                  result1 = null;
                }
                if (result1 !== null) {
                  result2 = parse_reg8();
                  if (result2 !== null) {
                    result3 = [];
                    result4 = parse___();
                    while (result4 !== null) {
                      result3.push(result4);
                      result4 = parse___();
                    }
                    if (result3 !== null) {
                      if (input.charCodeAt(pos) === 44) {
                        result4 = ",";
                        pos++;
                      } else {
                        result4 = null;
                        if (reportFailures === 0) {
                          matchFailed("\",\"");
                        }
                      }
                      if (result4 !== null) {
                        result5 = [];
                        result6 = parse___();
                        while (result6 !== null) {
                          result5.push(result6);
                          result6 = parse___();
                        }
                        if (result5 !== null) {
                          result6 = parse_reg8();
                          if (result6 !== null) {
                            result0 = [result0, result1, result2, result3, result4, result5, result6];
                          } else {
                            result0 = null;
                            pos = pos1;
                          }
                        } else {
                          result0 = null;
                          pos = pos1;
                        }
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                } else {
                  result0 = null;
                  pos = pos1;
                }
              } else {
                result0 = null;
                pos = pos1;
              }
              if (result0 !== null) {
                result0 = (function(offset, op, dst8, src8) {
                      return { command: "PUTINSTRDST8SRC8", op:op.toUpperCase(), dst8: dst8, src8: src8 };
                    })(pos0, result0[0], result0[2], result0[6]);
              }
              if (result0 === null) {
                pos = pos0;
              }
              if (result0 === null) {
                pos0 = pos;
                pos1 = pos;
                if (input.substr(pos, 3).toLowerCase() === "adi") {
                  result0 = input.substr(pos, 3);
                  pos += 3;
                } else {
                  result0 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"ADI\"");
                  }
                }
                if (result0 === null) {
                  if (input.substr(pos, 3).toLowerCase() === "aci") {
                    result0 = input.substr(pos, 3);
                    pos += 3;
                  } else {
                    result0 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"ACI\"");
                    }
                  }
                  if (result0 === null) {
                    if (input.substr(pos, 3).toLowerCase() === "sui") {
                      result0 = input.substr(pos, 3);
                      pos += 3;
                    } else {
                      result0 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"SUI\"");
                      }
                    }
                    if (result0 === null) {
                      if (input.substr(pos, 3).toLowerCase() === "sbi") {
                        result0 = input.substr(pos, 3);
                        pos += 3;
                      } else {
                        result0 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"SBI\"");
                        }
                      }
                      if (result0 === null) {
                        if (input.substr(pos, 3).toLowerCase() === "ani") {
                          result0 = input.substr(pos, 3);
                          pos += 3;
                        } else {
                          result0 = null;
                          if (reportFailures === 0) {
                            matchFailed("\"ANI\"");
                          }
                        }
                        if (result0 === null) {
                          if (input.substr(pos, 3).toLowerCase() === "xri") {
                            result0 = input.substr(pos, 3);
                            pos += 3;
                          } else {
                            result0 = null;
                            if (reportFailures === 0) {
                              matchFailed("\"XRI\"");
                            }
                          }
                          if (result0 === null) {
                            if (input.substr(pos, 3).toLowerCase() === "ori") {
                              result0 = input.substr(pos, 3);
                              pos += 3;
                            } else {
                              result0 = null;
                              if (reportFailures === 0) {
                                matchFailed("\"ORI\"");
                              }
                            }
                            if (result0 === null) {
                              if (input.substr(pos, 3).toLowerCase() === "cpi") {
                                result0 = input.substr(pos, 3);
                                pos += 3;
                              } else {
                                result0 = null;
                                if (reportFailures === 0) {
                                  matchFailed("\"CPI\"");
                                }
                              }
                              if (result0 === null) {
                                if (input.substr(pos, 2).toLowerCase() === "in") {
                                  result0 = input.substr(pos, 2);
                                  pos += 2;
                                } else {
                                  result0 = null;
                                  if (reportFailures === 0) {
                                    matchFailed("\"IN\"");
                                  }
                                }
                                if (result0 === null) {
                                  if (input.substr(pos, 3).toLowerCase() === "out") {
                                    result0 = input.substr(pos, 3);
                                    pos += 3;
                                  } else {
                                    result0 = null;
                                    if (reportFailures === 0) {
                                      matchFailed("\"OUT\"");
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
                if (result0 !== null) {
                  result2 = parse___();
                  if (result2 !== null) {
                    result1 = [];
                    while (result2 !== null) {
                      result1.push(result2);
                      result2 = parse___();
                    }
                  } else {
                    result1 = null;
                  }
                  if (result1 !== null) {
                    result2 = parse_expression();
                    if (result2 !== null) {
                      result0 = [result0, result1, result2];
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                } else {
                  result0 = null;
                  pos = pos1;
                }
                if (result0 !== null) {
                  result0 = (function(offset, op, imm8) {
                        return { command: "PUTINSTRIMM8", op:op.toUpperCase(), imm8:imm8 };
                      })(pos0, result0[0], result0[2]);
                }
                if (result0 === null) {
                  pos = pos0;
                }
                if (result0 === null) {
                  pos0 = pos;
                  pos1 = pos;
                  if (input.substr(pos, 3).toLowerCase() === "mvi") {
                    result0 = input.substr(pos, 3);
                    pos += 3;
                  } else {
                    result0 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"MVI\"");
                    }
                  }
                  if (result0 !== null) {
                    result2 = parse___();
                    if (result2 !== null) {
                      result1 = [];
                      while (result2 !== null) {
                        result1.push(result2);
                        result2 = parse___();
                      }
                    } else {
                      result1 = null;
                    }
                    if (result1 !== null) {
                      result2 = parse_reg8();
                      if (result2 !== null) {
                        result3 = [];
                        result4 = parse___();
                        while (result4 !== null) {
                          result3.push(result4);
                          result4 = parse___();
                        }
                        if (result3 !== null) {
                          if (input.charCodeAt(pos) === 44) {
                            result4 = ",";
                            pos++;
                          } else {
                            result4 = null;
                            if (reportFailures === 0) {
                              matchFailed("\",\"");
                            }
                          }
                          if (result4 !== null) {
                            result5 = [];
                            result6 = parse___();
                            while (result6 !== null) {
                              result5.push(result6);
                              result6 = parse___();
                            }
                            if (result5 !== null) {
                              result6 = parse_expression();
                              if (result6 !== null) {
                                result0 = [result0, result1, result2, result3, result4, result5, result6];
                              } else {
                                result0 = null;
                                pos = pos1;
                              }
                            } else {
                              result0 = null;
                              pos = pos1;
                            }
                          } else {
                            result0 = null;
                            pos = pos1;
                          }
                        } else {
                          result0 = null;
                          pos = pos1;
                        }
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                  if (result0 !== null) {
                    result0 = (function(offset, op, reg8, imm8) {
                          return { command: "PUTINSTRREG8IMM8", op:op.toUpperCase(), reg8:reg8, imm8: imm8 };
                        })(pos0, result0[0], result0[2], result0[6]);
                  }
                  if (result0 === null) {
                    pos = pos0;
                  }
                  if (result0 === null) {
                    pos0 = pos;
                    pos1 = pos;
                    if (input.substr(pos, 3).toLowerCase() === "inx") {
                      result0 = input.substr(pos, 3);
                      pos += 3;
                    } else {
                      result0 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"INX\"");
                      }
                    }
                    if (result0 === null) {
                      if (input.substr(pos, 3).toLowerCase() === "dcx") {
                        result0 = input.substr(pos, 3);
                        pos += 3;
                      } else {
                        result0 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"DCX\"");
                        }
                      }
                      if (result0 === null) {
                        if (input.substr(pos, 3).toLowerCase() === "dad") {
                          result0 = input.substr(pos, 3);
                          pos += 3;
                        } else {
                          result0 = null;
                          if (reportFailures === 0) {
                            matchFailed("\"DAD\"");
                          }
                        }
                      }
                    }
                    if (result0 !== null) {
                      result2 = parse___();
                      if (result2 !== null) {
                        result1 = [];
                        while (result2 !== null) {
                          result1.push(result2);
                          result2 = parse___();
                        }
                      } else {
                        result1 = null;
                      }
                      if (result1 !== null) {
                        result2 = parse_reg16_type1();
                        if (result2 !== null) {
                          result0 = [result0, result1, result2];
                        } else {
                          result0 = null;
                          pos = pos1;
                        }
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                    if (result0 !== null) {
                      result0 = (function(offset, op, reg16) {
                            return { command: "PUTINSTRREG16", op:op.toUpperCase(), reg16:reg16 };
                          })(pos0, result0[0], result0[2]);
                    }
                    if (result0 === null) {
                      pos = pos0;
                    }
                    if (result0 === null) {
                      pos0 = pos;
                      pos1 = pos;
                      if (input.substr(pos, 4).toLowerCase() === "push") {
                        result0 = input.substr(pos, 4);
                        pos += 4;
                      } else {
                        result0 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"PUSH\"");
                        }
                      }
                      if (result0 === null) {
                        if (input.substr(pos, 3).toLowerCase() === "pop") {
                          result0 = input.substr(pos, 3);
                          pos += 3;
                        } else {
                          result0 = null;
                          if (reportFailures === 0) {
                            matchFailed("\"POP\"");
                          }
                        }
                      }
                      if (result0 !== null) {
                        result2 = parse___();
                        if (result2 !== null) {
                          result1 = [];
                          while (result2 !== null) {
                            result1.push(result2);
                            result2 = parse___();
                          }
                        } else {
                          result1 = null;
                        }
                        if (result1 !== null) {
                          result2 = parse_reg16_type2();
                          if (result2 !== null) {
                            result0 = [result0, result1, result2];
                          } else {
                            result0 = null;
                            pos = pos1;
                          }
                        } else {
                          result0 = null;
                          pos = pos1;
                        }
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                      if (result0 !== null) {
                        result0 = (function(offset, op, reg16) {
                              return { command: "PUTINSTRREG16", op:op.toUpperCase(), reg16:reg16 };
                            })(pos0, result0[0], result0[2]);
                      }
                      if (result0 === null) {
                        pos = pos0;
                      }
                      if (result0 === null) {
                        pos0 = pos;
                        pos1 = pos;
                        if (input.substr(pos, 4).toLowerCase() === "ldax") {
                          result0 = input.substr(pos, 4);
                          pos += 4;
                        } else {
                          result0 = null;
                          if (reportFailures === 0) {
                            matchFailed("\"LDAX\"");
                          }
                        }
                        if (result0 === null) {
                          if (input.substr(pos, 4).toLowerCase() === "stax") {
                            result0 = input.substr(pos, 4);
                            pos += 4;
                          } else {
                            result0 = null;
                            if (reportFailures === 0) {
                              matchFailed("\"STAX\"");
                            }
                          }
                        }
                        if (result0 !== null) {
                          result2 = parse___();
                          if (result2 !== null) {
                            result1 = [];
                            while (result2 !== null) {
                              result1.push(result2);
                              result2 = parse___();
                            }
                          } else {
                            result1 = null;
                          }
                          if (result1 !== null) {
                            result2 = parse_reg16_type3();
                            if (result2 !== null) {
                              result0 = [result0, result1, result2];
                            } else {
                              result0 = null;
                              pos = pos1;
                            }
                          } else {
                            result0 = null;
                            pos = pos1;
                          }
                        } else {
                          result0 = null;
                          pos = pos1;
                        }
                        if (result0 !== null) {
                          result0 = (function(offset, op, reg16) {
                                return { command: "PUTINSTRREG16", op:op.toUpperCase(), reg16:reg16 };
                              })(pos0, result0[0], result0[2]);
                        }
                        if (result0 === null) {
                          pos = pos0;
                        }
                        if (result0 === null) {
                          pos0 = pos;
                          pos1 = pos;
                          if (input.substr(pos, 3).toLowerCase() === "jmp") {
                            result0 = input.substr(pos, 3);
                            pos += 3;
                          } else {
                            result0 = null;
                            if (reportFailures === 0) {
                              matchFailed("\"JMP\"");
                            }
                          }
                          if (result0 === null) {
                            if (input.substr(pos, 3).toLowerCase() === "jnz") {
                              result0 = input.substr(pos, 3);
                              pos += 3;
                            } else {
                              result0 = null;
                              if (reportFailures === 0) {
                                matchFailed("\"JNZ\"");
                              }
                            }
                            if (result0 === null) {
                              if (input.substr(pos, 2).toLowerCase() === "jz") {
                                result0 = input.substr(pos, 2);
                                pos += 2;
                              } else {
                                result0 = null;
                                if (reportFailures === 0) {
                                  matchFailed("\"JZ\"");
                                }
                              }
                              if (result0 === null) {
                                if (input.substr(pos, 3).toLowerCase() === "jnc") {
                                  result0 = input.substr(pos, 3);
                                  pos += 3;
                                } else {
                                  result0 = null;
                                  if (reportFailures === 0) {
                                    matchFailed("\"JNC\"");
                                  }
                                }
                                if (result0 === null) {
                                  if (input.substr(pos, 2).toLowerCase() === "jc") {
                                    result0 = input.substr(pos, 2);
                                    pos += 2;
                                  } else {
                                    result0 = null;
                                    if (reportFailures === 0) {
                                      matchFailed("\"JC\"");
                                    }
                                  }
                                  if (result0 === null) {
                                    if (input.substr(pos, 3).toLowerCase() === "jpo") {
                                      result0 = input.substr(pos, 3);
                                      pos += 3;
                                    } else {
                                      result0 = null;
                                      if (reportFailures === 0) {
                                        matchFailed("\"JPO\"");
                                      }
                                    }
                                    if (result0 === null) {
                                      if (input.substr(pos, 3).toLowerCase() === "jpe") {
                                        result0 = input.substr(pos, 3);
                                        pos += 3;
                                      } else {
                                        result0 = null;
                                        if (reportFailures === 0) {
                                          matchFailed("\"JPE\"");
                                        }
                                      }
                                      if (result0 === null) {
                                        if (input.substr(pos, 2).toLowerCase() === "jp") {
                                          result0 = input.substr(pos, 2);
                                          pos += 2;
                                        } else {
                                          result0 = null;
                                          if (reportFailures === 0) {
                                            matchFailed("\"JP\"");
                                          }
                                        }
                                        if (result0 === null) {
                                          if (input.substr(pos, 2).toLowerCase() === "jm") {
                                            result0 = input.substr(pos, 2);
                                            pos += 2;
                                          } else {
                                            result0 = null;
                                            if (reportFailures === 0) {
                                              matchFailed("\"JM\"");
                                            }
                                          }
                                          if (result0 === null) {
                                            if (input.substr(pos, 4).toLowerCase() === "call") {
                                              result0 = input.substr(pos, 4);
                                              pos += 4;
                                            } else {
                                              result0 = null;
                                              if (reportFailures === 0) {
                                                matchFailed("\"CALL\"");
                                              }
                                            }
                                            if (result0 === null) {
                                              if (input.substr(pos, 3).toLowerCase() === "cnz") {
                                                result0 = input.substr(pos, 3);
                                                pos += 3;
                                              } else {
                                                result0 = null;
                                                if (reportFailures === 0) {
                                                  matchFailed("\"CNZ\"");
                                                }
                                              }
                                              if (result0 === null) {
                                                if (input.substr(pos, 2).toLowerCase() === "cz") {
                                                  result0 = input.substr(pos, 2);
                                                  pos += 2;
                                                } else {
                                                  result0 = null;
                                                  if (reportFailures === 0) {
                                                    matchFailed("\"CZ\"");
                                                  }
                                                }
                                                if (result0 === null) {
                                                  if (input.substr(pos, 3).toLowerCase() === "cnc") {
                                                    result0 = input.substr(pos, 3);
                                                    pos += 3;
                                                  } else {
                                                    result0 = null;
                                                    if (reportFailures === 0) {
                                                      matchFailed("\"CNC\"");
                                                    }
                                                  }
                                                  if (result0 === null) {
                                                    if (input.substr(pos, 2).toLowerCase() === "cc") {
                                                      result0 = input.substr(pos, 2);
                                                      pos += 2;
                                                    } else {
                                                      result0 = null;
                                                      if (reportFailures === 0) {
                                                        matchFailed("\"CC\"");
                                                      }
                                                    }
                                                    if (result0 === null) {
                                                      if (input.substr(pos, 3).toLowerCase() === "cpo") {
                                                        result0 = input.substr(pos, 3);
                                                        pos += 3;
                                                      } else {
                                                        result0 = null;
                                                        if (reportFailures === 0) {
                                                          matchFailed("\"CPO\"");
                                                        }
                                                      }
                                                      if (result0 === null) {
                                                        if (input.substr(pos, 3).toLowerCase() === "cpe") {
                                                          result0 = input.substr(pos, 3);
                                                          pos += 3;
                                                        } else {
                                                          result0 = null;
                                                          if (reportFailures === 0) {
                                                            matchFailed("\"CPE\"");
                                                          }
                                                        }
                                                        if (result0 === null) {
                                                          if (input.substr(pos, 2).toLowerCase() === "cp") {
                                                            result0 = input.substr(pos, 2);
                                                            pos += 2;
                                                          } else {
                                                            result0 = null;
                                                            if (reportFailures === 0) {
                                                              matchFailed("\"CP\"");
                                                            }
                                                          }
                                                          if (result0 === null) {
                                                            if (input.substr(pos, 2).toLowerCase() === "cm") {
                                                              result0 = input.substr(pos, 2);
                                                              pos += 2;
                                                            } else {
                                                              result0 = null;
                                                              if (reportFailures === 0) {
                                                                matchFailed("\"CM\"");
                                                              }
                                                            }
                                                            if (result0 === null) {
                                                              if (input.substr(pos, 3).toLowerCase() === "lda") {
                                                                result0 = input.substr(pos, 3);
                                                                pos += 3;
                                                              } else {
                                                                result0 = null;
                                                                if (reportFailures === 0) {
                                                                  matchFailed("\"LDA\"");
                                                                }
                                                              }
                                                              if (result0 === null) {
                                                                if (input.substr(pos, 3).toLowerCase() === "sta") {
                                                                  result0 = input.substr(pos, 3);
                                                                  pos += 3;
                                                                } else {
                                                                  result0 = null;
                                                                  if (reportFailures === 0) {
                                                                    matchFailed("\"STA\"");
                                                                  }
                                                                }
                                                                if (result0 === null) {
                                                                  if (input.substr(pos, 4).toLowerCase() === "lhld") {
                                                                    result0 = input.substr(pos, 4);
                                                                    pos += 4;
                                                                  } else {
                                                                    result0 = null;
                                                                    if (reportFailures === 0) {
                                                                      matchFailed("\"LHLD\"");
                                                                    }
                                                                  }
                                                                  if (result0 === null) {
                                                                    if (input.substr(pos, 4).toLowerCase() === "shld") {
                                                                      result0 = input.substr(pos, 4);
                                                                      pos += 4;
                                                                    } else {
                                                                      result0 = null;
                                                                      if (reportFailures === 0) {
                                                                        matchFailed("\"SHLD\"");
                                                                      }
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                          if (result0 !== null) {
                            result2 = parse___();
                            if (result2 !== null) {
                              result1 = [];
                              while (result2 !== null) {
                                result1.push(result2);
                                result2 = parse___();
                              }
                            } else {
                              result1 = null;
                            }
                            if (result1 !== null) {
                              result2 = parse_expression();
                              if (result2 !== null) {
                                result0 = [result0, result1, result2];
                              } else {
                                result0 = null;
                                pos = pos1;
                              }
                            } else {
                              result0 = null;
                              pos = pos1;
                            }
                          } else {
                            result0 = null;
                            pos = pos1;
                          }
                          if (result0 !== null) {
                            result0 = (function(offset, op, imm16) {
                                  return { command: "PUTINSTRIMM16", op:op.toUpperCase(), imm16:imm16 };
                                })(pos0, result0[0], result0[2]);
                          }
                          if (result0 === null) {
                            pos = pos0;
                          }
                          if (result0 === null) {
                            pos0 = pos;
                            pos1 = pos;
                            if (input.substr(pos, 3).toLowerCase() === "lxi") {
                              result0 = input.substr(pos, 3);
                              pos += 3;
                            } else {
                              result0 = null;
                              if (reportFailures === 0) {
                                matchFailed("\"LXI\"");
                              }
                            }
                            if (result0 !== null) {
                              result2 = parse___();
                              if (result2 !== null) {
                                result1 = [];
                                while (result2 !== null) {
                                  result1.push(result2);
                                  result2 = parse___();
                                }
                              } else {
                                result1 = null;
                              }
                              if (result1 !== null) {
                                result2 = parse_reg16_type1();
                                if (result2 !== null) {
                                  result3 = [];
                                  result4 = parse___();
                                  while (result4 !== null) {
                                    result3.push(result4);
                                    result4 = parse___();
                                  }
                                  if (result3 !== null) {
                                    if (input.charCodeAt(pos) === 44) {
                                      result4 = ",";
                                      pos++;
                                    } else {
                                      result4 = null;
                                      if (reportFailures === 0) {
                                        matchFailed("\",\"");
                                      }
                                    }
                                    if (result4 !== null) {
                                      result5 = [];
                                      result6 = parse___();
                                      while (result6 !== null) {
                                        result5.push(result6);
                                        result6 = parse___();
                                      }
                                      if (result5 !== null) {
                                        result6 = parse_expression();
                                        if (result6 !== null) {
                                          result0 = [result0, result1, result2, result3, result4, result5, result6];
                                        } else {
                                          result0 = null;
                                          pos = pos1;
                                        }
                                      } else {
                                        result0 = null;
                                        pos = pos1;
                                      }
                                    } else {
                                      result0 = null;
                                      pos = pos1;
                                    }
                                  } else {
                                    result0 = null;
                                    pos = pos1;
                                  }
                                } else {
                                  result0 = null;
                                  pos = pos1;
                                }
                              } else {
                                result0 = null;
                                pos = pos1;
                              }
                            } else {
                              result0 = null;
                              pos = pos1;
                            }
                            if (result0 !== null) {
                              result0 = (function(offset, op, reg16, imm16) {
                                    return { command: "PUTINSTRREG16IMM16", op:op.toUpperCase(), reg16:reg16, imm16: imm16 };
                                  })(pos0, result0[0], result0[2], result0[6]);
                            }
                            if (result0 === null) {
                              pos = pos0;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        return result0;
      }
      
      function parse_reg8() {
        var result0;
        var pos0;
        
        pos0 = pos;
        if (input.substr(pos, 1).toLowerCase() === "a") {
          result0 = input.substr(pos, 1);
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"A\"");
          }
        }
        if (result0 === null) {
          if (input.substr(pos, 1).toLowerCase() === "b") {
            result0 = input.substr(pos, 1);
            pos++;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"B\"");
            }
          }
          if (result0 === null) {
            if (input.substr(pos, 1).toLowerCase() === "c") {
              result0 = input.substr(pos, 1);
              pos++;
            } else {
              result0 = null;
              if (reportFailures === 0) {
                matchFailed("\"C\"");
              }
            }
            if (result0 === null) {
              if (input.substr(pos, 1).toLowerCase() === "d") {
                result0 = input.substr(pos, 1);
                pos++;
              } else {
                result0 = null;
                if (reportFailures === 0) {
                  matchFailed("\"D\"");
                }
              }
              if (result0 === null) {
                if (input.substr(pos, 1).toLowerCase() === "e") {
                  result0 = input.substr(pos, 1);
                  pos++;
                } else {
                  result0 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"E\"");
                  }
                }
                if (result0 === null) {
                  if (input.substr(pos, 1).toLowerCase() === "h") {
                    result0 = input.substr(pos, 1);
                    pos++;
                  } else {
                    result0 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"H\"");
                    }
                  }
                  if (result0 === null) {
                    if (input.substr(pos, 1).toLowerCase() === "l") {
                      result0 = input.substr(pos, 1);
                      pos++;
                    } else {
                      result0 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"L\"");
                      }
                    }
                    if (result0 === null) {
                      if (input.substr(pos, 1).toLowerCase() === "m") {
                        result0 = input.substr(pos, 1);
                        pos++;
                      } else {
                        result0 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"M\"");
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, reg8) { return reg8.toUpperCase(); })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_reg16_type1() {
        var result0;
        var pos0;
        
        pos0 = pos;
        if (input.substr(pos, 1).toLowerCase() === "b") {
          result0 = input.substr(pos, 1);
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"B\"");
          }
        }
        if (result0 === null) {
          if (input.substr(pos, 1).toLowerCase() === "d") {
            result0 = input.substr(pos, 1);
            pos++;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"D\"");
            }
          }
          if (result0 === null) {
            if (input.substr(pos, 1).toLowerCase() === "h") {
              result0 = input.substr(pos, 1);
              pos++;
            } else {
              result0 = null;
              if (reportFailures === 0) {
                matchFailed("\"H\"");
              }
            }
            if (result0 === null) {
              if (input.substr(pos, 2).toLowerCase() === "sp") {
                result0 = input.substr(pos, 2);
                pos += 2;
              } else {
                result0 = null;
                if (reportFailures === 0) {
                  matchFailed("\"SP\"");
                }
              }
            }
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, reg16) { return reg16.toUpperCase(); })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_reg16_type2() {
        var result0;
        var pos0;
        
        pos0 = pos;
        if (input.substr(pos, 1).toLowerCase() === "b") {
          result0 = input.substr(pos, 1);
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"B\"");
          }
        }
        if (result0 === null) {
          if (input.substr(pos, 1).toLowerCase() === "d") {
            result0 = input.substr(pos, 1);
            pos++;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"D\"");
            }
          }
          if (result0 === null) {
            if (input.substr(pos, 1).toLowerCase() === "h") {
              result0 = input.substr(pos, 1);
              pos++;
            } else {
              result0 = null;
              if (reportFailures === 0) {
                matchFailed("\"H\"");
              }
            }
            if (result0 === null) {
              if (input.substr(pos, 3).toLowerCase() === "psw") {
                result0 = input.substr(pos, 3);
                pos += 3;
              } else {
                result0 = null;
                if (reportFailures === 0) {
                  matchFailed("\"PSW\"");
                }
              }
            }
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, reg16) { return reg16.toUpperCase(); })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_reg16_type3() {
        var result0;
        var pos0;
        
        pos0 = pos;
        if (input.substr(pos, 1).toLowerCase() === "b") {
          result0 = input.substr(pos, 1);
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"B\"");
          }
        }
        if (result0 === null) {
          if (input.substr(pos, 1).toLowerCase() === "d") {
            result0 = input.substr(pos, 1);
            pos++;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"D\"");
            }
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, reg16) { return reg16.toUpperCase(); })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse___() {
        var result0;
        
        if (/^[ \t]/.test(input.charAt(pos))) {
          result0 = input.charAt(pos);
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("[ \\t]");
          }
        }
        return result0;
      }
      
      function parse_comment() {
        var result0, result1, result2;
        var pos0;
        
        pos0 = pos;
        if (input.charCodeAt(pos) === 59) {
          result0 = ";";
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\";\"");
          }
        }
        if (result0 !== null) {
          result1 = [];
          if (/^[^\n]/.test(input.charAt(pos))) {
            result2 = input.charAt(pos);
            pos++;
          } else {
            result2 = null;
            if (reportFailures === 0) {
              matchFailed("[^\\n]");
            }
          }
          while (result2 !== null) {
            result1.push(result2);
            if (/^[^\n]/.test(input.charAt(pos))) {
              result2 = input.charAt(pos);
              pos++;
            } else {
              result2 = null;
              if (reportFailures === 0) {
                matchFailed("[^\\n]");
              }
            }
          }
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos0;
          }
        } else {
          result0 = null;
          pos = pos0;
        }
        return result0;
      }
      
      function parse_expression() {
        var result0, result1, result2, result3;
        var pos0, pos1, pos2;
        
        pos0 = pos;
        pos1 = pos;
        result0 = parse_expressionstrong();
        if (result0 !== null) {
          result1 = [];
          pos2 = pos;
          result2 = parse_weakop();
          if (result2 !== null) {
            result3 = parse_expressionstrong();
            if (result3 !== null) {
              result2 = [result2, result3];
            } else {
              result2 = null;
              pos = pos2;
            }
          } else {
            result2 = null;
            pos = pos2;
          }
          while (result2 !== null) {
            result1.push(result2);
            pos2 = pos;
            result2 = parse_weakop();
            if (result2 !== null) {
              result3 = parse_expressionstrong();
              if (result3 !== null) {
                result2 = [result2, result3];
              } else {
                result2 = null;
                pos = pos2;
              }
            } else {
              result2 = null;
              pos = pos2;
            }
          }
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, left, exprs) {
            var r = left;
            for (var i = 0; i < exprs.length; i++)
              r = new BinaryOpExpression(r, exprs[i][0], exprs[i][1]);
            return r;
          })(pos0, result0[0], result0[1]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_expressionstrong() {
        var result0, result1, result2, result3;
        var pos0, pos1, pos2;
        
        pos0 = pos;
        pos1 = pos;
        result0 = parse_expressionelement();
        if (result0 !== null) {
          result1 = [];
          pos2 = pos;
          result2 = parse_strongop();
          if (result2 !== null) {
            result3 = parse_expressionelement();
            if (result3 !== null) {
              result2 = [result2, result3];
            } else {
              result2 = null;
              pos = pos2;
            }
          } else {
            result2 = null;
            pos = pos2;
          }
          while (result2 !== null) {
            result1.push(result2);
            pos2 = pos;
            result2 = parse_strongop();
            if (result2 !== null) {
              result3 = parse_expressionelement();
              if (result3 !== null) {
                result2 = [result2, result3];
              } else {
                result2 = null;
                pos = pos2;
              }
            } else {
              result2 = null;
              pos = pos2;
            }
          }
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, left, exprs) {
            var r = left;
            for (var i = 0; i < exprs.length; i++)
              r = new BinaryOpExpression(r, exprs[i][0], exprs[i][1]);
            return r;
          })(pos0, result0[0], result0[1]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_expressionelement() {
        var result0, result1, result2, result3;
        var pos0, pos1, pos2;
        
        pos0 = pos;
        pos1 = pos;
        result0 = [];
        result1 = parse_sign();
        while (result1 !== null) {
          result0.push(result1);
          result1 = parse_sign();
        }
        if (result0 !== null) {
          result1 = parse_character();
          if (result1 === null) {
            result1 = parse_hexnumber();
            if (result1 === null) {
              result1 = parse_binarynumber();
              if (result1 === null) {
                result1 = parse_octalnumber();
                if (result1 === null) {
                  result1 = parse_decnumber();
                  if (result1 === null) {
                    result1 = parse_identifier();
                    if (result1 === null) {
                      pos2 = pos;
                      if (input.charCodeAt(pos) === 40) {
                        result1 = "(";
                        pos++;
                      } else {
                        result1 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"(\"");
                        }
                      }
                      if (result1 !== null) {
                        result2 = parse_expression();
                        if (result2 !== null) {
                          if (input.charCodeAt(pos) === 41) {
                            result3 = ")";
                            pos++;
                          } else {
                            result3 = null;
                            if (reportFailures === 0) {
                              matchFailed("\")\"");
                            }
                          }
                          if (result3 !== null) {
                            result1 = [result1, result2, result3];
                          } else {
                            result1 = null;
                            pos = pos2;
                          }
                        } else {
                          result1 = null;
                          pos = pos2;
                        }
                      } else {
                        result1 = null;
                        pos = pos2;
                      }
                    }
                  }
                }
              }
            }
          }
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, signs, value) {
            var r = value;
            for (var i = 0; i < signs.length; i++)
              r = new UnaryOpExpression(signs[i], r);
            return r;
          })(pos0, result0[0], result0[1]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_weakop() {
        var result0;
        
        if (input.charCodeAt(pos) === 43) {
          result0 = "+";
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"+\"");
          }
        }
        if (result0 === null) {
          if (input.charCodeAt(pos) === 45) {
            result0 = "-";
            pos++;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"-\"");
            }
          }
          if (result0 === null) {
            if (input.charCodeAt(pos) === 38) {
              result0 = "&";
              pos++;
            } else {
              result0 = null;
              if (reportFailures === 0) {
                matchFailed("\"&\"");
              }
            }
            if (result0 === null) {
              if (input.charCodeAt(pos) === 124) {
                result0 = "|";
                pos++;
              } else {
                result0 = null;
                if (reportFailures === 0) {
                  matchFailed("\"|\"");
                }
              }
              if (result0 === null) {
                if (input.charCodeAt(pos) === 94) {
                  result0 = "^";
                  pos++;
                } else {
                  result0 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"^\"");
                  }
                }
              }
            }
          }
        }
        return result0;
      }
      
      function parse_strongop() {
        var result0;
        
        if (input.charCodeAt(pos) === 42) {
          result0 = "*";
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"*\"");
          }
        }
        if (result0 === null) {
          if (input.charCodeAt(pos) === 47) {
            result0 = "/";
            pos++;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"/\"");
            }
          }
        }
        return result0;
      }
      
      function parse_sign() {
        var result0;
        
        if (input.charCodeAt(pos) === 43) {
          result0 = "+";
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"+\"");
          }
        }
        if (result0 === null) {
          if (input.charCodeAt(pos) === 45) {
            result0 = "-";
            pos++;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"-\"");
            }
          }
        }
        return result0;
      }
      
      function parse_identifier() {
        var result0, result1, result2;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        if (/^[A-Za-z_]/.test(input.charAt(pos))) {
          result0 = input.charAt(pos);
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("[A-Za-z_]");
          }
        }
        if (result0 !== null) {
          result1 = [];
          if (/^[A-Za-z0-9_]/.test(input.charAt(pos))) {
            result2 = input.charAt(pos);
            pos++;
          } else {
            result2 = null;
            if (reportFailures === 0) {
              matchFailed("[A-Za-z0-9_]");
            }
          }
          while (result2 !== null) {
            result1.push(result2);
            if (/^[A-Za-z0-9_]/.test(input.charAt(pos))) {
              result2 = input.charAt(pos);
              pos++;
            } else {
              result2 = null;
              if (reportFailures === 0) {
                matchFailed("[A-Za-z0-9_]");
              }
            }
          }
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, first, next) { return new IdentifierExpression((first+next.join("")).toUpperCase()); })(pos0, result0[0], result0[1]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_character() {
        var result0, result1, result2;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        if (input.charCodeAt(pos) === 39) {
          result0 = "'";
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("\"'\"");
          }
        }
        if (result0 !== null) {
          result1 = parse_characterLiteral();
          if (result1 !== null) {
            if (input.charCodeAt(pos) === 39) {
              result2 = "'";
              pos++;
            } else {
              result2 = null;
              if (reportFailures === 0) {
                matchFailed("\"'\"");
              }
            }
            if (result2 !== null) {
              result0 = [result0, result1, result2];
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, character) { return new ValueExpression(character.charCodeAt(0)); })(pos0, result0[1]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_characterLiteral() {
        var result0, result1, result2, result3;
        var pos0, pos1, pos2;
        
        pos0 = pos;
        if (/^[ !#$%&()*+,-.\/0-9:;<=>?@A-Z[\]^_`a-z{|}~]/.test(input.charAt(pos))) {
          result0 = input.charAt(pos);
          pos++;
        } else {
          result0 = null;
          if (reportFailures === 0) {
            matchFailed("[ !#$%&()*+,-.\\/0-9:;<=>?@A-Z[\\]^_`a-z{|}~]");
          }
        }
        if (result0 !== null) {
          result0 = (function(offset, char) { return char; })(pos0, result0);
        }
        if (result0 === null) {
          pos = pos0;
        }
        if (result0 === null) {
          pos0 = pos;
          pos1 = pos;
          if (input.charCodeAt(pos) === 92) {
            result0 = "\\";
            pos++;
          } else {
            result0 = null;
            if (reportFailures === 0) {
              matchFailed("\"\\\\\"");
            }
          }
          if (result0 !== null) {
            if (input.charCodeAt(pos) === 110) {
              result1 = "n";
              pos++;
            } else {
              result1 = null;
              if (reportFailures === 0) {
                matchFailed("\"n\"");
              }
            }
            if (result1 !== null) {
              result0 = [result0, result1];
            } else {
              result0 = null;
              pos = pos1;
            }
          } else {
            result0 = null;
            pos = pos1;
          }
          if (result0 !== null) {
            result0 = (function(offset) { return '\n'; })(pos0);
          }
          if (result0 === null) {
            pos = pos0;
          }
          if (result0 === null) {
            pos0 = pos;
            pos1 = pos;
            if (input.charCodeAt(pos) === 92) {
              result0 = "\\";
              pos++;
            } else {
              result0 = null;
              if (reportFailures === 0) {
                matchFailed("\"\\\\\"");
              }
            }
            if (result0 !== null) {
              if (input.charCodeAt(pos) === 114) {
                result1 = "r";
                pos++;
              } else {
                result1 = null;
                if (reportFailures === 0) {
                  matchFailed("\"r\"");
                }
              }
              if (result1 !== null) {
                result0 = [result0, result1];
              } else {
                result0 = null;
                pos = pos1;
              }
            } else {
              result0 = null;
              pos = pos1;
            }
            if (result0 !== null) {
              result0 = (function(offset) { return '\r'; })(pos0);
            }
            if (result0 === null) {
              pos = pos0;
            }
            if (result0 === null) {
              pos0 = pos;
              pos1 = pos;
              if (input.charCodeAt(pos) === 92) {
                result0 = "\\";
                pos++;
              } else {
                result0 = null;
                if (reportFailures === 0) {
                  matchFailed("\"\\\\\"");
                }
              }
              if (result0 !== null) {
                if (input.charCodeAt(pos) === 116) {
                  result1 = "t";
                  pos++;
                } else {
                  result1 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"t\"");
                  }
                }
                if (result1 !== null) {
                  result0 = [result0, result1];
                } else {
                  result0 = null;
                  pos = pos1;
                }
              } else {
                result0 = null;
                pos = pos1;
              }
              if (result0 !== null) {
                result0 = (function(offset) { return '\t'; })(pos0);
              }
              if (result0 === null) {
                pos = pos0;
              }
              if (result0 === null) {
                pos0 = pos;
                pos1 = pos;
                if (input.charCodeAt(pos) === 92) {
                  result0 = "\\";
                  pos++;
                } else {
                  result0 = null;
                  if (reportFailures === 0) {
                    matchFailed("\"\\\\\"");
                  }
                }
                if (result0 !== null) {
                  if (input.charCodeAt(pos) === 92) {
                    result1 = "\\";
                    pos++;
                  } else {
                    result1 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"\\\\\"");
                    }
                  }
                  if (result1 !== null) {
                    result0 = [result0, result1];
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                } else {
                  result0 = null;
                  pos = pos1;
                }
                if (result0 !== null) {
                  result0 = (function(offset) { return '\\'; })(pos0);
                }
                if (result0 === null) {
                  pos = pos0;
                }
                if (result0 === null) {
                  pos0 = pos;
                  pos1 = pos;
                  if (input.charCodeAt(pos) === 92) {
                    result0 = "\\";
                    pos++;
                  } else {
                    result0 = null;
                    if (reportFailures === 0) {
                      matchFailed("\"\\\\\"");
                    }
                  }
                  if (result0 !== null) {
                    if (input.charCodeAt(pos) === 39) {
                      result1 = "'";
                      pos++;
                    } else {
                      result1 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"'\"");
                      }
                    }
                    if (result1 !== null) {
                      result0 = [result0, result1];
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                  } else {
                    result0 = null;
                    pos = pos1;
                  }
                  if (result0 !== null) {
                    result0 = (function(offset) { return '\''; })(pos0);
                  }
                  if (result0 === null) {
                    pos = pos0;
                  }
                  if (result0 === null) {
                    pos0 = pos;
                    pos1 = pos;
                    if (input.charCodeAt(pos) === 92) {
                      result0 = "\\";
                      pos++;
                    } else {
                      result0 = null;
                      if (reportFailures === 0) {
                        matchFailed("\"\\\\\"");
                      }
                    }
                    if (result0 !== null) {
                      if (input.charCodeAt(pos) === 34) {
                        result1 = "\"";
                        pos++;
                      } else {
                        result1 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"\\\"\"");
                        }
                      }
                      if (result1 !== null) {
                        result0 = [result0, result1];
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                    } else {
                      result0 = null;
                      pos = pos1;
                    }
                    if (result0 !== null) {
                      result0 = (function(offset) { return '\"'; })(pos0);
                    }
                    if (result0 === null) {
                      pos = pos0;
                    }
                    if (result0 === null) {
                      pos0 = pos;
                      pos1 = pos;
                      if (input.charCodeAt(pos) === 92) {
                        result0 = "\\";
                        pos++;
                      } else {
                        result0 = null;
                        if (reportFailures === 0) {
                          matchFailed("\"\\\\\"");
                        }
                      }
                      if (result0 !== null) {
                        if (input.charCodeAt(pos) === 120) {
                          result1 = "x";
                          pos++;
                        } else {
                          result1 = null;
                          if (reportFailures === 0) {
                            matchFailed("\"x\"");
                          }
                        }
                        if (result1 !== null) {
                          pos2 = pos;
                          if (/^[0-7]/.test(input.charAt(pos))) {
                            result2 = input.charAt(pos);
                            pos++;
                          } else {
                            result2 = null;
                            if (reportFailures === 0) {
                              matchFailed("[0-7]");
                            }
                          }
                          if (result2 !== null) {
                            if (/^[0-9A-Fa-f]/.test(input.charAt(pos))) {
                              result3 = input.charAt(pos);
                              pos++;
                            } else {
                              result3 = null;
                              if (reportFailures === 0) {
                                matchFailed("[0-9A-Fa-f]");
                              }
                            }
                            if (result3 !== null) {
                              result2 = [result2, result3];
                            } else {
                              result2 = null;
                              pos = pos2;
                            }
                          } else {
                            result2 = null;
                            pos = pos2;
                          }
                          if (result2 !== null) {
                            result0 = [result0, result1, result2];
                          } else {
                            result0 = null;
                            pos = pos1;
                          }
                        } else {
                          result0 = null;
                          pos = pos1;
                        }
                      } else {
                        result0 = null;
                        pos = pos1;
                      }
                      if (result0 !== null) {
                        result0 = (function(offset, hexEscape) { return String.fromCharCode(parseInt(hexEscape.join(""),16)); })(pos0, result0[2]);
                      }
                      if (result0 === null) {
                        pos = pos0;
                      }
                    }
                  }
                }
              }
            }
          }
        }
        return result0;
      }
      
      function parse_hexnumber() {
        var result0, result1;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        if (/^[0-9A-Fa-f]/.test(input.charAt(pos))) {
          result1 = input.charAt(pos);
          pos++;
        } else {
          result1 = null;
          if (reportFailures === 0) {
            matchFailed("[0-9A-Fa-f]");
          }
        }
        if (result1 !== null) {
          result0 = [];
          while (result1 !== null) {
            result0.push(result1);
            if (/^[0-9A-Fa-f]/.test(input.charAt(pos))) {
              result1 = input.charAt(pos);
              pos++;
            } else {
              result1 = null;
              if (reportFailures === 0) {
                matchFailed("[0-9A-Fa-f]");
              }
            }
          }
        } else {
          result0 = null;
        }
        if (result0 !== null) {
          if (input.substr(pos, 1).toLowerCase() === "h") {
            result1 = input.substr(pos, 1);
            pos++;
          } else {
            result1 = null;
            if (reportFailures === 0) {
              matchFailed("\"H\"");
            }
          }
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, digits) { return new ValueExpression(parseInt(digits.join(""), 16)); })(pos0, result0[0]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_binarynumber() {
        var result0, result1;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        if (/^[0-1]/.test(input.charAt(pos))) {
          result1 = input.charAt(pos);
          pos++;
        } else {
          result1 = null;
          if (reportFailures === 0) {
            matchFailed("[0-1]");
          }
        }
        if (result1 !== null) {
          result0 = [];
          while (result1 !== null) {
            result0.push(result1);
            if (/^[0-1]/.test(input.charAt(pos))) {
              result1 = input.charAt(pos);
              pos++;
            } else {
              result1 = null;
              if (reportFailures === 0) {
                matchFailed("[0-1]");
              }
            }
          }
        } else {
          result0 = null;
        }
        if (result0 !== null) {
          if (input.substr(pos, 1).toLowerCase() === "b") {
            result1 = input.substr(pos, 1);
            pos++;
          } else {
            result1 = null;
            if (reportFailures === 0) {
              matchFailed("\"B\"");
            }
          }
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, digits) { return new ValueExpression(parseInt(digits.join(""), 2)); })(pos0, result0[0]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_octalnumber() {
        var result0, result1;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        if (/^[0-7]/.test(input.charAt(pos))) {
          result1 = input.charAt(pos);
          pos++;
        } else {
          result1 = null;
          if (reportFailures === 0) {
            matchFailed("[0-7]");
          }
        }
        if (result1 !== null) {
          result0 = [];
          while (result1 !== null) {
            result0.push(result1);
            if (/^[0-7]/.test(input.charAt(pos))) {
              result1 = input.charAt(pos);
              pos++;
            } else {
              result1 = null;
              if (reportFailures === 0) {
                matchFailed("[0-7]");
              }
            }
          }
        } else {
          result0 = null;
        }
        if (result0 !== null) {
          if (input.substr(pos, 1).toLowerCase() === "o") {
            result1 = input.substr(pos, 1);
            pos++;
          } else {
            result1 = null;
            if (reportFailures === 0) {
              matchFailed("\"O\"");
            }
          }
          if (result1 === null) {
            if (input.substr(pos, 1).toLowerCase() === "q") {
              result1 = input.substr(pos, 1);
              pos++;
            } else {
              result1 = null;
              if (reportFailures === 0) {
                matchFailed("\"Q\"");
              }
            }
          }
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, digits) { return new ValueExpression(parseInt(digits.join(""),8)); })(pos0, result0[0]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      function parse_decnumber() {
        var result0, result1;
        var pos0, pos1;
        
        pos0 = pos;
        pos1 = pos;
        if (/^[0-9]/.test(input.charAt(pos))) {
          result1 = input.charAt(pos);
          pos++;
        } else {
          result1 = null;
          if (reportFailures === 0) {
            matchFailed("[0-9]");
          }
        }
        if (result1 !== null) {
          result0 = [];
          while (result1 !== null) {
            result0.push(result1);
            if (/^[0-9]/.test(input.charAt(pos))) {
              result1 = input.charAt(pos);
              pos++;
            } else {
              result1 = null;
              if (reportFailures === 0) {
                matchFailed("[0-9]");
              }
            }
          }
        } else {
          result0 = null;
        }
        if (result0 !== null) {
          if (input.substr(pos, 1).toLowerCase() === "d") {
            result1 = input.substr(pos, 1);
            pos++;
          } else {
            result1 = null;
            if (reportFailures === 0) {
              matchFailed("\"D\"");
            }
          }
          result1 = result1 !== null ? result1 : "";
          if (result1 !== null) {
            result0 = [result0, result1];
          } else {
            result0 = null;
            pos = pos1;
          }
        } else {
          result0 = null;
          pos = pos1;
        }
        if (result0 !== null) {
          result0 = (function(offset, digits) { return new ValueExpression(parseInt(digits.join(""),10)); })(pos0, result0[0]);
        }
        if (result0 === null) {
          pos = pos0;
        }
        return result0;
      }
      
      
      function cleanupExpected(expected) {
        expected.sort();
        
        var lastExpected = null;
        var cleanExpected = [];
        for (var i = 0; i < expected.length; i++) {
          if (expected[i] !== lastExpected) {
            cleanExpected.push(expected[i]);
            lastExpected = expected[i];
          }
        }
        return cleanExpected;
      }
      
      function computeErrorPosition() {
        /*
         * The first idea was to use |String.split| to break the input up to the
         * error position along newlines and derive the line and column from
         * there. However IE's |split| implementation is so broken that it was
         * enough to prevent it.
         */
        
        var line = 1;
        var column = 1;
        var seenCR = false;
        
        for (var i = 0; i < Math.max(pos, rightmostFailuresPos); i++) {
          var ch = input.charAt(i);
          if (ch === "\n") {
            if (!seenCR) { line++; }
            column = 1;
            seenCR = false;
          } else if (ch === "\r" || ch === "\u2028" || ch === "\u2029") {
            line++;
            column = 1;
            seenCR = true;
          } else {
            column++;
            seenCR = false;
          }
        }
        
        return { line: line, column: column };
      }
      
      
        /* /-------------\
         * | EXPRESSIONS |
         * \-------------/ */
        // TODO hack, but I need this outside the parser
        window.UnaryOpExpression = function(op, expr) {
          this.op = op;
          this.expr = expr;
        }
        window.BinaryOpExpression = function(left, op, right) {
          this.left = left;
          this.op = op;
          this.right = right;
        }
        window.ValueExpression = function(value) {
          this.value = value;
        }
        window.IdentifierExpression = function(ident) {
          this.ident = ident;
        }
      
      
      var result = parseFunctions[startRule]();
      
      /*
       * The parser is now in one of the following three states:
       *
       * 1. The parser successfully parsed the whole input.
       *
       *    - |result !== null|
       *    - |pos === input.length|
       *    - |rightmostFailuresExpected| may or may not contain something
       *
       * 2. The parser successfully parsed only a part of the input.
       *
       *    - |result !== null|
       *    - |pos < input.length|
       *    - |rightmostFailuresExpected| may or may not contain something
       *
       * 3. The parser did not successfully parse any part of the input.
       *
       *   - |result === null|
       *   - |pos === 0|
       *   - |rightmostFailuresExpected| contains at least one failure
       *
       * All code following this comment (including called functions) must
       * handle these states.
       */
      if (result === null || pos !== input.length) {
        var offset = Math.max(pos, rightmostFailuresPos);
        var found = offset < input.length ? input.charAt(offset) : null;
        var errorPosition = computeErrorPosition();
        
        throw new this.SyntaxError(
          cleanupExpected(rightmostFailuresExpected),
          found,
          offset,
          errorPosition.line,
          errorPosition.column
        );
      }
      
      return result;
    },
    
    /* Returns the parser source code. */
    toSource: function() { return this._source; }
  };
  
  /* Thrown when a parser encounters a syntax error. */
  
  result.SyntaxError = function(expected, found, offset, line, column) {
    function buildMessage(expected, found) {
      var expectedHumanized, foundHumanized;
      
      switch (expected.length) {
        case 0:
          expectedHumanized = "end of input";
          break;
        case 1:
          expectedHumanized = expected[0];
          break;
        default:
          expectedHumanized = expected.slice(0, expected.length - 1).join(", ")
            + " or "
            + expected[expected.length - 1];
      }
      
      foundHumanized = found ? quote(found) : "end of input";
      
      return "Expected " + expectedHumanized + " but " + foundHumanized + " found.";
    }
    
    this.name = "SyntaxError";
    this.expected = expected;
    this.found = found;
    this.message = buildMessage(expected, found);
    this.offset = offset;
    this.line = line;
    this.column = column;
  };
  
  result.SyntaxError.prototype = Error.prototype;
  
  return result;
})();